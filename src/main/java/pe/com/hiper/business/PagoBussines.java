package pe.com.hiper.business;

import java.util.HashMap;

public interface PagoBussines {

	public HashMap<String, String> habilitarIngresoEfectivo(String id_producto, String id_cliente, double costoServicio,
			int timeOut, String trace, int recarga, int flagInicio);
	
	public HashMap<String, Object> habilitarIngresoDeBilletes(String trace);
	
	HashMap<String, String> detenerIngresoEfectivo(String trace);
	
	HashMap<String, Object> consultarEfectivoIngresado(String trace);
	
	public HashMap<String, String> cancelarUltimoComando(String trace);
	
	public HashMap<String, Object> procesarPago(String trace, String tipoPago, String nombre_cliente, String dni,
			String telefono, String correo, String descProducto, String nro_documento, String flagTPP,
			String codigoVerificacion);
	
	public HashMap<String, String> entregarDinero(String trace, int adicionales, boolean isCancel);
	
	public HashMap<String, String> cancelarPago(String trace, String proDescripcion, String codigoVerificacion);
	
	
}
