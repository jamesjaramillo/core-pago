package pe.com.hiper.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import pe.com.hiper.common.Constantes;
import pe.com.hiper.common.UtilWeb;
import pe.com.hiper.entity.TpMontoPagado;
import pe.com.hiper.entity.TpMontoPagar;
import pe.com.hiper.model.Moneda;

@Component("PagoBussinesNV200")
public class PagoBussinesNVImpl extends PagoBussinesImpl {

	private static final Logger LOGGER = LogManager.getLogger(PagoBussinesNVImpl.class);

	@Value("${monedero.monedas.max}")
	private int cantidadMaximaMonedas;

	@Value("${kiosco.moneda}")
	private String monedaPais;

	@Value("${kiosco.terminal}")
	private String nroTerminal;
	
	public double MONTO_BILLETES;
	public static double TEMP_MONTO_BILLETES=0;
	
	public static int ingresandoBillete=0;

	public HashMap<String, String> habilitarIngresoEfectivo(String id_producto, String id_cliente, double costoServicio,
			int timeOut, String trace, int recarga, int flagInicio) {
		LOGGER.info(trace + " INICIO habilitarIngresoEfectivo");
		HashMap<String, String> dato = new HashMap<String, String>();

		UtilWeb.MONTO_INGRESADO = 0;
		MONTO_BILLETES = 0;
		TEMP_MONTO_BILLETES = 0;
		UtilWeb.VUELTO_BILLETES=new ArrayList<Moneda>();

		TpMontoPagar montoPagar = montoPagarService.getMontoPagar(trace);
		if (montoPagar == null) {
			UtilWeb.MONTO_INGRESADO = 0;
			montoPagar = new TpMontoPagar();
			montoPagar.setnCostoServicio(costoServicio);
			montoPagar.setdFechaIngreso(new Date());
			montoPagar.setcCodCliente(id_cliente);
			montoPagar.setnCodProducto(id_producto);
			montoPagar.setcTrace(trace);
			montoPagarService.saveMontoPagar(montoPagar);
		} else {
			// System.out.println("TpMontoPagar -> " + montoPagar.toString());
			List<TpMontoPagado> listaMontoPagados = montoPagadoService.listarMontoPagado(trace);
			UtilWeb.MONTO_INGRESADO = (listaMontoPagados.stream().mapToDouble(monto -> monto.getnDenominacion()).sum()
					+ UtilWeb.MONTO_INGRESADO);
		}
		System.out.println("*****************************************");
		System.out.println("*****************************************");
		System.out.println("EL MONTO INGRESADO ES " + UtilWeb.MONTO_INGRESADO);
		dato.put("estado", "0");
		dato.put("descripcion", "ok");

		if (costoServicio > 0) {

			dato.put("descripcionAux", String.valueOf(cantidadMaximaMonedas));
			iniciarAceptadorMonedas(trace, costoServicio);
			iniciarAceptadorBilletes(trace);
		}
		return dato;
	}
	
	public HashMap<String, Object> consultarEfectivoIngresado(String trace) {		
		
		HashMap<String, Object> dataBilletes=obtenerBilletesIngresados(trace);
		if(dataBilletes.containsKey("billetes")){
			dataBilletes.put("billetes", dataBilletes.get("billetes"));
		}
		System.out.println("UtilWeb.MONTO_INGRESADO -> "+UtilWeb.MONTO_INGRESADO+"\t MONTO_BILLETES -> "+MONTO_BILLETES);
		dataBilletes.put("estado", "1");
		dataBilletes.put("descripcion", "ok");
		dataBilletes.put("monto", UtilWeb.MONTO_INGRESADO + MONTO_BILLETES);
		dataBilletes.put("estadoDispositivo", String.valueOf(ingresandoBillete));	
		
		return dataBilletes;
	}
	
	@Override
	public HashMap<String, Object> habilitarIngresoDeBilletes(String trace) {
		this.LOGGER.info(trace+" "+"Executing metodo habilitarIngresoDeBilletes()");
		iniciarAceptadorBilletes(trace);
		HashMap<String, Object> data=new HashMap<String, Object>();
		
		data.put("estado", "0");
		data.put("descripcion","Billtero habilitado");
		
		
		return data;
	}
	
	@Override
	public HashMap<String, String> detenerIngresoEfectivo(String trace) {
		HashMap<String, String> data = new HashMap<String, String>();
		detenerAceptadorMonedas();
		detenerAceptadorBilletes(trace);
		
		data.put("estado", "1");
		data.put("descripcion", "Servicio ok");

		return data;
	}
	
	private void detenerAceptadorBilletes(String trace) {
		try {
			evaCoreService.invocarServicioGenerico(trace, Constantes.EVACORE_BILLETERO_CANCELAR_SCROW);
			Thread.sleep(500);
			//this.getDispositivoService().finalizarScrow(trace, false);
			evaCoreService.finalizarScrow(trace, false);
		} catch (Exception e) {
			this.LOGGER.error("Error al detenerAceptadorbilletes ", e);
		}
	}

	private void iniciarAceptadorBilletes(String trace) {
		evaCoreService.aceptarBilletes(trace);
	}
	
	private HashMap<String, Object> obtenerBilletesIngresados(String trace){
		this.LOGGER.info(trace+" "+"Executing metodo obtenerBilletesIngresados()");
		HashMap<String, Object> data=new HashMap<String, Object>();
		//Moneda data=new Moneda();
		try {
			List<Moneda> dataBilletes=new ArrayList<Moneda>();
			//Moneda billetesIngresados=this.getDispositivoService().billeteroConsultarIngreso(trace);
			Moneda billetesIngresados=evaCoreService.billetesIngresados(trace);
			this.LOGGER.info(trace+" "+"Obtener billetes ingresados "+billetesIngresados);
			MONTO_BILLETES = 0;
			if(billetesIngresados.getEstado()==0){
				dataBilletes=billetesIngresados.getBilletes();
				for (Moneda moneda : dataBilletes) {
					MONTO_BILLETES+=moneda.getCantidad() * moneda.getDenominacion();
				}
				/*if(MONTO_BILLETES!=TEMP_MONTO_BILLETES){
					isBilleteIngresado=true;
				}*/
				TEMP_MONTO_BILLETES=MONTO_BILLETES;
				if(Boolean.parseBoolean(billetesIngresados.getIdNote())){
					ingresandoBillete=1;
				}
				else{
					ingresandoBillete=0;
				}
				data.put("billetes", dataBilletes);
				
			}
			else{
				
			}
		} catch (Exception e) {
			/*data.put("estado", "-1");
			data.put("descripcion", "Error al obtener los billtes");
			data.put("monto", "0.0");*/
			LOGGER.error("Error obtenerBilletesIngresados ", e);
		}
		
		this.LOGGER.info(trace+" "+"resultado obtenerBilletesIngresados "+data);
		return data;
	}
}
