package pe.com.hiper.business;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import pe.com.hiper.common.UtilWeb;
import pe.com.hiper.service.HCService;

@Component
public class HostClienteBusiness {
	private static final Logger LOGGER = LogManager.getLogger(HostClienteBusiness.class);

	@Autowired
	HCService hcService;

	@Value("${kiosco.terminal}")
	private String nroTerminal;

	public HashMap<String, String> obtenerCostoProducto(String trace, String codigoServicio, String nroCupon) {
		LOGGER.info(trace + " INICIO habilitarIngresoEfectivo");
		HashMap<String, String> data = new HashMap<String, String>();
		HashMap<String, String> objetoJson = hcService.obtenerCostoProducto(trace, codigoServicio, nroCupon);
		if (objetoJson.get("estado").equals("0")) {
			if (objetoJson.containsKey("response_code") && objetoJson.get("response_code").equals("00")) {
				data.put("estado", "0");
				data.put("descripcion", "ok");
				data.put("fechaIngreso", objetoJson.get("fecha"));
				data.put("fechaSalida", objetoJson.get("fechaSalida"));
				data.put("nroTicket", objetoJson.get("numero_ticket"));
				// TEMP_NRO_TICKET = objetoJson.getString("numero_ticket");
				UtilWeb.NRO_TICKET = objetoJson.get("numero_ticket");
				data.put("codigoEntrada", objetoJson.get("estacionEntrada"));
				data.put("monto", objetoJson.get("monto"));
				data.put("impuesto", objetoJson.get("impuesto"));
				data.put("montoTotal", objetoJson.get("monto_total"));
				int tiempo = UtilWeb.minutosDiferencia(objetoJson.get("fecha"), objetoJson.get("fechaSalida"));
				data.put("tiempo", String.valueOf(tiempo));

				if (nroCupon.length() == 12) {
					data.put("payPassBalance", objetoJson.get("payPassBalance"));
					data.put("payPassConsumo", objetoJson.get("payPassConsumo"));
					data.put("payPassAmountOld", objetoJson.get("payPassAmountOld"));
					data.put("payPassCard", objetoJson.get("payPassCard"));
					data.put("payPassName", objetoJson.get("payPassName"));
					data.put("payPassDni", objetoJson.get("payPassDni"));
					data.put("payPassTrace", objetoJson.get("payPassTrace"));
				}
			} else if (objetoJson.containsKey("errorCode")) {
				data.put("estado", objetoJson.get("errorCode"));
				data.put("descripcion", objetoJson.get("messageHost"));
			} else {
				data.put("estado", "2");
				data.put("descripcion", "Problemas con hipercenter");
			}
		} else {
			data.put("estado", "1");
			data.put("descripcion", "Error al conectarse al servicio de Hipercenter");
		}

		return data;
	}

	public HashMap<String, String> pagoPortales(String trace, String nroTicket, String nroCupon, String fechaIngreso,
			String fechaSalida, String stacionEntrada, String monto, String impuesto, String montoTotal,
			String tipoPago, String numOperacion, String numAutorizacion, String cardNumberEnc, String cardRead,
			String flagNtaCredito, String montoNtaCredito, String idTraxCastle, String fechaTrxCastle) {

		HashMap<String, String> dataHc = new HashMap<String, String>();
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			// String terminal = this.getReadProperty().getString("terminal");

			String estadoCastle = "";
			String numSerieCastle = "";

			if (!numAutorizacion.equals("") && !numOperacion.equals("")) {
				HashMap<String, String> rptEstado = null;// getDispositivoService().obtenerDatosCastle(trace);
				estadoCastle = rptEstado.get("estado");
				numSerieCastle = rptEstado.get("numSerie");
			}

			dataHc = hcService.pagoPortales(trace, nroTerminal, nroTicket, nroCupon, fechaIngreso, fechaSalida,
					stacionEntrada, monto, impuesto, montoTotal, tipoPago, numOperacion, numAutorizacion, cardNumberEnc,
					cardRead, flagNtaCredito, montoNtaCredito, idTraxCastle, fechaTrxCastle, estadoCastle,
					numSerieCastle);
			if (dataHc.get("estado").equals("0")) {
				if (dataHc.get("response_code").equals("00")) {
					data.put("estado", "0");
					data.put("descripcion", "ok");
				} else {
					data.put("estado", "2");
					data.put("descripcion", "Problemas con hipercenter");
				}
			} else {
				data.put("estado", "1");
				data.put("descripcion", "Error al conectarse al servicio de Hipercenter");
			}
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error al conectarse al servicio de Hipercenter");
			e.printStackTrace();
		}
		this.LOGGER.info(trace + " " + "Finalizar metodo pagoPortales()");
		return data;
	}

	public HashMap<String, String> cancelarPagoPortales(String trace, String nroTicket, String nroCupon,
			String fechaIngreso, String fechaSalida, String stacionEntrada, String monto, String impuesto,
			String montoTotal) {
		HashMap<String, String> data = new HashMap<String, String>();
		HashMap<String, String> objetoJson = hcService.pagoPortales(trace, nroTerminal, nroTicket, nroCupon,
				fechaIngreso, fechaSalida, stacionEntrada, monto, impuesto, montoTotal, "3", "", "", "", "", "0",
				"0.00", "", "", "", "");

		if (objetoJson.get("estado").equals("0")) {
			if (objetoJson.get("response_code").equals("00")) {
				data.put("estado", "0");
				data.put("descripcion", "ok");
			} else {
				data.put("estado", "2");
				data.put("descripcion", "Problemas con hipercenter");
			}
		} else {
			data.put("estado", "1");
			data.put("descripcion", "Error al conectarse al servicio de Hipercenter");
		}
		return data;

	}

	public HashMap<String, String> voucherPortales(String trace, String service_name, String payPassBalance,
			String payPassConsumo, String payPassAmountOld, String payPassCard, String payPassName, String payPassDni,
			String payPassTrace) {
		HashMap<String, String> data = new HashMap<String, String>();
		HashMap<String, String> objetoJson = hcService.voucherPortales(payPassTrace, nroTerminal, service_name,
				payPassBalance, payPassConsumo, payPassAmountOld, payPassCard, payPassName, payPassDni, payPassTrace);

		if (objetoJson.get("estado").equals("0")) {
			if (objetoJson.get("response_code").equals("00")) {
				data.put("estado", "0");
				data.put("descripcion", "ok");
				data.put("print_data", objetoJson.get("print_data"));
			} else {
				data.put("estado", "2");
				data.put("descripcion", "Problemas con hipercenter");
			}
		} else {
			data.put("estado", "1");
			data.put("descripcion", "Error al conectarse al servicio de Hipercenter");
		}
		return data;

	}

	public HashMap<String, String> consultaRucPortales(String trace, String ruc, String tipoOperacion, String nombre,
			String calle, String codigoPosptal, String ciudad) {
		HashMap<String, String> data = new HashMap<String, String>();
		HashMap<String, String> objetoJson=hcService.consultaRucPortales(trace, nroTerminal, ruc, tipoOperacion, nombre, calle, codigoPosptal, ciudad); //this.getHiperCenterService().consultaRucPortales(trace, terminal, ruc, tipoOperacion, nombre, calle, codigoPosptal, ciudad);
		if(objetoJson.get("estado").equals("0")){
			if(objetoJson.get("response_code").equals("00")){
				data.put("estado", "0");
				data.put("descripcion", "ok");
				data.put("ciudad", objetoJson.get("city"));
				data.put("codigoPostal", objetoJson.get("postalCode"));
				data.put("nombre", objetoJson.get("name"));
				data.put("calle", objetoJson.get("street1"));
				data.put("estadoServicio", objetoJson.get("approval_code"));
			}
			else{
				data.put("estado", "2");
				data.put("descripcion", "Problemas con hipercenter");
			}
		}
		else{
			data.put("estado", "1");
			data.put("descripcion", "Error al conectarse al servicio de Hipercenter");
		}
		return data;
	}

}
