package pe.com.hiper.business;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


import pe.com.hiper.common.CalcularVueltoBilletes;
import pe.com.hiper.common.Constantes;
import pe.com.hiper.common.DispensarMonedas;
import pe.com.hiper.common.UtilMoneda;
import pe.com.hiper.common.UtilWeb;
import pe.com.hiper.entity.TmDevice;
import pe.com.hiper.entity.TpMontoPagado;
import pe.com.hiper.entity.TpMontoPagar;
import pe.com.hiper.model.Moneda;
import pe.com.hiper.service.DeviceService;
import pe.com.hiper.service.EvaCoreService;
import pe.com.hiper.service.HCService;
import pe.com.hiper.service.MontoPagadoService;
import pe.com.hiper.service.MontoPagarService;
import pe.com.hiper.service.TransactionLogService;

@Component("PagoBussinesFujitsu")
public class PagoBussinesImpl implements PagoBussines {

	private static final Logger LOGGER = LogManager.getLogger(PagoBussinesImpl.class);
	@Autowired
	MontoPagadoService montoPagadoService;

	@Autowired
	MontoPagarService montoPagarService;

	@Autowired
	EvaCoreService evaCoreService;

	@Autowired
	DeviceService deviceService;

	@Autowired
	HCService hcService;
	
	@Autowired
	TransactionLogService transactionService;

	@Autowired
	private ModelMapper modelMapper;

	@Value("${monedero.monedas.max}")
	private int cantidadMaximaMonedas;

	@Value("${kiosco.moneda}")
	private String monedaPais;

	@Value("${kiosco.terminal}")
	private String nroTerminal;

	@Value("${smartHopper.monedas.max}")
	private int cantidadMaximaSH;

	@Value("${hcenter.agencia}")
	private String agencia;
	
	@Value("${hopper.dispensar.cantidad.max}")
	private int hooperCantidadMax;
	
	@Value("${dispositivo.hopper.canal.hopper1}")
	private int direccionHopper;
	
	@Value("${kiosco.tipoAceptador}")
	private String tipoAceptador;

	private static ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	private TimerTask taskMonederoPolling;
	private ScheduledFuture<?> resultTareaMonedero = null;

	private final int BILLETES = 1;
	private final int MONEDAS = 2;

	private final int BILLETES_CASHBOX = 0;
	private final int BILLETES_STACKER = 1;

	public HashMap<String, String> habilitarIngresoEfectivo(String id_producto, String id_cliente, double costoServicio,
			int timeOut, String trace, int recarga, int flagInicio) {
		LOGGER.info(trace + " INICIO habilitarIngresoEfectivo");
		HashMap<String, String> dato = new HashMap<String, String>();
		TpMontoPagar montoPagar = montoPagarService.getMontoPagar(trace);
		UtilWeb.MONTO_INGRESADO = 0;

		UtilWeb.VUELTO_BILLETES=new ArrayList<Moneda>();
		if (montoPagar == null) {
			UtilWeb.MONTO_INGRESADO = 0;
			montoPagar = new TpMontoPagar();
			montoPagar.setnCostoServicio(costoServicio);
			montoPagar.setdFechaIngreso(new Date());
			montoPagar.setcCodCliente(id_cliente);
			montoPagar.setnCodProducto(id_producto);
			montoPagar.setcTrace(trace);
			montoPagarService.saveMontoPagar(montoPagar);
		} else {
			// System.out.println("TpMontoPagar -> " + montoPagar.toString());
			List<TpMontoPagado> listaMontoPagados = montoPagadoService.listarMontoPagado(trace);
			UtilWeb.MONTO_INGRESADO = (listaMontoPagados.stream().mapToDouble(monto -> monto.getnDenominacion()).sum()
					+ UtilWeb.MONTO_INGRESADO);
		}
		System.out.println("MONTO INGRESADO -> " + UtilWeb.MONTO_INGRESADO);
		dato.put("estado", "0");
		dato.put("descripcion", "ok");
		iniciarAceptadorMonedas(trace, costoServicio);
		return dato;
	}

	public HashMap<String, Object> habilitarIngresoDeBilletes(String trace) {
		Moneda moneda = new Moneda();
		HashMap<String, Object> data = new HashMap<String, Object>();
		TpMontoPagado montoPagado = null;
		try {
			moneda = evaCoreService.aceptarBilletes(trace);
			if (moneda.getEstado() == 0) {
				data.put("estado", "0");
				data.put("descripcion", "OK");
				List<Moneda> billetes = moneda.getBilletes();
				data.put("monto", UtilWeb.MONTO_INGRESADO);
				data.put("billetes", billetes);
				for (Moneda billetePay : billetes) {
					for (int j = 0; j < billetePay.getCantidadPayout(); j++) {
						montoPagado = new TpMontoPagado();
						montoPagado.setnTipo(BILLETES);
						montoPagado.setnDenominacion(Double.parseDouble(String.valueOf(billetePay.getDenominacion())));
						montoPagado.setcTrace(trace);
						montoPagado.setBpayout(1);
						montoPagadoService.saveMontoPagado(montoPagado);
					}
				}
				for (Moneda billeteCashBox : billetes) {

					for (int k = 0; k < billeteCashBox.getCantidadCashbox(); k++) {
						montoPagado.setnTipo(BILLETES);
						montoPagado
								.setnDenominacion(Double.parseDouble(String.valueOf(billeteCashBox.getDenominacion())));
						montoPagado.setcTrace(trace);
						montoPagado.setBpayout(0);
						montoPagadoService.saveMontoPagado(montoPagado);
					}
				}
//				
			} else {
				data.put("estado", "1");
				data.put("descripcion", moneda.getDescripcion());
				data.put("monto", 0.0);
				data.put("billetes", moneda.getBilletes());
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("Finalizo");
		}

		return data;
	}

	public HashMap<String, String> detenerIngresoEfectivo(String trace) {
		HashMap<String, String> data = new HashMap<String, String>();
		detenerAceptadorMonedas();
		data.put("estado", "1");
		data.put("descripcion", "Servicio ok");

		return data;
	}

	public HashMap<String, Object> consultarEfectivoIngresado(String trace) {
		HashMap<String, Object> data = new HashMap<String, Object>();

		data.put("estado", "1");
		data.put("descripcion", "ok");
		data.put("monto", UtilWeb.MONTO_INGRESADO);
		return data;
	}
	
	public HashMap<String, String> cancelarUltimoComando(String trace) {
		HashMap<String, String> data = new HashMap<String, String>();

		data=evaCoreService.invocarServicioGenerico(trace, Constantes.EVACORE_BILLETERO_CANCELAR_UTL);
		return data;
	}

	public HashMap<String, Object> procesarPago(String trace, String tipoPago, String nombre_cliente, String dni,
			String telefono, String correo, String descProducto, String nro_documento, String flagTPP,
			String codigoVerificacion) {
		UtilWeb.VUELTO_MONEDAS=0.0;
		HashMap<String, Object> retValue = new HashMap<String, Object>();

		if (!UtilWeb.TRACE_PROCESADO.equals(trace)) {

			UtilWeb.TRACE_PROCESADO = trace;
			// flagCancelar=false;
			registrarBilletes(trace); //utilizado cuando es nv200

			Double montoVuelto = 0.0;
			
			// ingresoMonedas=false;

			String flagRecicla = "0";// = this.getDispositivoService().isReciclador(trace) ? "0" : "1";

			int denominador = 1;
			String monedaPais = this.monedaPais;
			if (monedaPais.equals("COP")) {
				denominador = 100;
			}

			try {
				double montoPagadoTotal = 0.0;

				// MontoPagar montoPagar =
				// this.getMontoPagarDAO().obtenerMontoPagarPorTrace(this.getConn(), trace);
				TpMontoPagar montoPagar = montoPagarService.getMontoPagar(trace);
				double costoServicio = montoPagar.getnCostoServicio();

				// List<MontoPagado> montoPagadoList =
				// this.getMontoPagadoDAO().obtenerMontoPagadoPorTrace(this.getConn(), trace);
				List<TpMontoPagado> montoPagadoList = montoPagadoService.listarMontoPagado(trace);				

				HashMap<String, Object> dataDenominacion = UtilMoneda.obtenerDenominacionIngresada(montoPagadoList);
				HashMap<Double, Integer> monedas = (HashMap<Double, Integer>) dataDenominacion
						.get(UtilMoneda.DENO_MONEDAS);
				HashMap<Integer, Integer> billetes = (HashMap<Integer, Integer>) dataDenominacion
						.get(UtilMoneda.DENO_BILLETES_CASHBOX);
				HashMap<Integer, Integer> billetesPayout = (HashMap<Integer, Integer>) dataDenominacion
						.get(UtilMoneda.DENO_BILLETES_PAYOUT);
				montoPagadoTotal = (double) dataDenominacion.get(UtilMoneda.TOTAL_INGRESADO);

				System.out.println("*******MONEDAS******");
				System.out.println(monedas);
				System.out.println("*******BILLETES******");
				System.out.println(billetes);
				System.out.println("*******BILLETES PAUOUT******");
				System.out.println(billetesPayout);

				// montoPagadoTotal = montoPagadoTotal + montoPagoParcial;

				// System.out.println(finalizaScrow);
				/* FIN INGRESAR LOS BILLETES AL SCROW */
				boolean flagVuelto = false;
				System.out.println("montoPagadoTotal -> " + montoPagadoTotal);
				System.out.println("getnCostoServicio -> " + montoPagar.getnCostoServicio());
				if (montoPagadoTotal >= montoPagar.getnCostoServicio()) {

					CalcularVueltoBilletes objetoVuelto = new CalcularVueltoBilletes();
					List<TmDevice> listaDispositivos = deviceService.obtenerDispositivos("PY01");
					List<TmDevice> resultado = objetoVuelto.calcularDenominacionBilletes(
							String.valueOf(montoPagar.getnCostoServicio()), String.valueOf(montoPagadoTotal),
							listaDispositivos);

					double saldoMonedas = 0.0;

					for (TmDevice device : resultado) {
						if (device.getcDsCodeDenomination().equals("saldoMonedas")) {
							saldoMonedas = Double.parseDouble(device.getnDsQuantityDenomination().replace(",", "."));
							if (saldoMonedas > 0) {
								flagVuelto = true;
							}
						} else if (!device.getcDsCodeDenomination().equals("vuelto")) {
							UtilWeb.VUELTO_BILLETES.add(new Moneda(Integer.parseInt(device.getcDsCodeDenomination()),
									Integer.parseInt(device.getnDsQuantityDenomination()), device.getcDsMoneyCode()));
							System.out.println("VUELTO A REGISTRAR -> device.getcDsMoneyCode() "+device.getcDsMoneyCode()+"\t device.getcDsCodeDenomination() "+device.getcDsCodeDenomination());
							actualizarCantidadMonedasDB("PY01", String.valueOf(Integer.parseInt(device.getcDsCodeDenomination()) * 100), Integer.parseInt(device.getnDsQuantityDenomination()) * -1);
							flagVuelto = true;
						}
					}

					System.out.println("DISPENSAR " + saldoMonedas + " EN MONEDAS");

					String camino = "1";
					if (!monedas.isEmpty()) {
						System.out.println("SE INGRESARON MONEDAS");
						camino = derivarMonedas(trace);

					}

					String formatBilletesCashbox = UtilMoneda.getFormatBilletes(billetes, deviceService.obtenerDispositivos("CB01"));
					System.out.println("formatBilletesCashbox -> " + formatBilletesCashbox);

					String formatBilletesPayout = UtilMoneda.getFormatBilletes(billetesPayout, listaDispositivos);
					System.out.println("formatBilletesPayout -> " + formatBilletesPayout);

					HashMap<String, String> resultPago = hcService.procesarPagoTotalNuevo(
							montoPagar.getnCostoServicio(), montoPagadoTotal, montoPagar.getnCodProducto(), dni,
							formatBilletesCashbox, "", trace, "", "", "", descProducto, agencia, nroTerminal, flagTPP,
							flagRecicla, formatBilletesPayout, camino, "", "", "", "");

					if (resultPago.get("response_code").equals("00")) {
						System.out.println("INGRESO AL RESPONSE CODE-> "+tipoAceptador);
						System.out.println("BILLETES CASHBOX "+billetes+" \t "+billetes.isEmpty());
						System.out.println("BILLETES PAYOUT "+billetesPayout+" \t "+billetesPayout.isEmpty());
						if (tipoAceptador.equals("4")) {
							// AGREGAR METODOS PARA FINALIZAR SCROW DEL BILLETERO
						
							evaCoreService.finalizarScrow(trace, false);
						}
						actualizarCantidadMonedasSM(trace, monedas);
						retValue.put("textoImprimir", resultPago.get("print_data"));
						retValue.put("textoImprimirVuelto", resultPago.get("print_data"));

						retValue.put("vuelto", flagVuelto ? "0" : "1");
						retValue.put("vueltoMonedas", saldoMonedas);
						UtilWeb.VUELTO_MONEDAS=saldoMonedas;
						retValue.put("estado", "0");
						retValue.put("descripcion", "pago realizado con exito");
						System.out.println("NRO DE TRACE "+trace+"\t nroTerminal -> "+nroTerminal);
						transactionService.actualizarMonedasIngresada(trace, nroTerminal, UtilMoneda.getFormatMonedas(monedas, deviceService.obtenerDispositivos("SH01")));
						System.out.println("ACTUALIZAR MONEDAS OK ");
						

					} else {

						retValue.put("vuelto", "1");
						retValue.put("estado", "1");
						retValue.put("descripcion", "No se pudo procesar el pago");
					}

					/*
					 * JSONObject resultPagoTotal =
					 * this.getHiperCenterService().procesarPagoTotalNuevo(montoPagar.
					 * getCostoServicio(), montoPagadoTotal, montoPagar.getIdServicio(), dni,
					 * formatBilletesCashBox(billetes), "", trace, hoppers[0], hoppers[1],
					 * hoppers[2], descProducto, this.getReadProperty().getString("agencia"),
					 * this.getReadProperty().getString("terminal"), flagTPP, flagRecicla,
					 * formatBilletesPayout(billetesPayout), camino, "", "" , "", "");
					 * System.out.println("resultPagoTotal -> "+resultPagoTotal);
					 * if(resultPagoTotal.get("response_code").equals("00")){
					 * retValue.setTextoImprimir(resultPagoTotal.getString("print_data"));
					 * this.LOGGER.info(trace+" "+"costoServicio "+costoServicio);
					 * if(tipoAceptador.equals("4")){ if(costoServicio>0){
					 * this.LOGGER.info(trace+" "+"finalizarScrow  "+costoServicio);
					 * this.getDispositivoService().finalizarScrow(trace, false);
					 * 
					 * } }
					 * 
					 * 
					 * String monedasCadena[]=formatMonedasSH(monedas).split(":"); for(int i=0;
					 * i<monedasCadena.length; i++){ String
					 * denoMoneda[]=monedasCadena[i].split(";");
					 * actualizarCantidadDispositivo(terminal, Integer.parseInt(denoMoneda[1]),
					 * denoMoneda[0], deviceCode, denominador); }
					 * retValue.setTextoImprimir(resultPagoTotal.getString("print_data"));
					 * retValue.setTextoImprimirVuelto(resultPagoTotal.getString("print_data"));
					 * //this.getDispositivoService().agregarMonedas(trace, cantidad, denominacion);
					 * retValue.setVuelto(flagVuelto?"0":"1"); retValue.setEstado("0");
					 * retValue.setDescripcion("pago realizado con exito");
					 * getTransactionLogDAO().registrarVueltoPago(getConn(), trace, terminal,
					 * "nTxCardNumber2",formatMonedasSH(monedas)); } else{ retValue.setVuelto("1");
					 * retValue.setEstado("1");
					 * retValue.setDescripcion("No se pudo procesar el pago");
					 * 
					 * }
					 */
				}

				/*
				 * if(!flagVuelto){ boolean flagMontoPagado =
				 * this.getMontoPagadoDAO().eliminarMontoPagado(this.getConn()); boolean
				 * flagMontoPagar = this.getMontoPagarDAO().eliminarMontoPagar(this.getConn());
				 * 
				 * } componenteLog.sethTxServiceOutput(UtilFuncion.getHoraActual());
				 */

			} catch (Exception e) {
				/*
				 * componenteLog.sethTxServiceOutput(UtilFuncion.getHoraActual());
				 * componenteLog.setbTxError(false); LOGGER.error(trace+" "+"Estado: " +
				 * retValue.getEstado() + " Error: ", e);
				 */
				e.printStackTrace();
			} finally {
				// socketCliente.cerrarSession();
			}
		} else {
			/*
			 * componenteLog.sethTxServiceOutput(UtilFuncion.getHoraActual());
			 * retValue.setEstado("0");
			 * retValue.setDescripcion("eL NRO DE TRACE YA HA SIDO PROCESADO");
			 * this.LOGGER.info(trace+" EL NRO DE TRACE YA HA SIDO PROCESADO");
			 */
		}
		/*
		 * TpComponenteLogDAO componentDAO = new TpComponenteLogImplSQL();
		 * componentDAO.guardarComponente(componenteLog);
		 */

		return retValue;
	}
	
	public HashMap<String, String> entregarDinero(String trace, int adicionales, boolean isCancel){
		this.LOGGER.info(trace+" "+"INICIO METODO entregarDinero");
		String textoImprimir="";
		int denominador=1;
		double montoVuelto=UtilWeb.VUELTO_MONEDAS;
		System.out.println("MONTO DE VUELTO -> "+montoVuelto);
		BigDecimal montoPendiente=new BigDecimal(0);
		
		if(monedaPais.equals("COP")){
			denominador=100;
		}
		
		int denominacionHopper=0;
		int cantidadHopper=0;		
		
		if(hooperCantidadMax>0){
			try {
				List<TmDevice> monedasHopper=deviceService.obtenerDispositivos("HO01");
				cantidadHopper = monedasHopper.stream().mapToInt(d -> Integer.parseInt(d.getnDsQuantityDenomination())).sum();
				denominacionHopper = Integer.parseInt(monedasHopper.get(0).getcDsCodeDenomination());
				System.out.println("cantidadHopper -> "+cantidadHopper+"\t denominacionHopper- > "+denominacionHopper);				
			}
			catch (Exception e) {
				LOGGER.error(trace+" "+"Error en el método consultarCantidadDispositivo()", e);
			}
			
		}
		List<TpMontoPagado> montoPagadoList = montoPagadoService.listarMontoPagado(trace);
		TpMontoPagar montoPagor = montoPagarService.getMontoPagar(trace);
		//considerar a borrar
		
		double totalIngresado=montoPagadoList.stream().mapToDouble(TpMontoPagado::getnDenominacion).sum();
		if(isCancel){
			montoVuelto=totalIngresado;
		}
		
		System.out.println("entro a metodo entregar dinero");
		this.LOGGER.info(trace+" "+"Inicio metodo entregarDinero()");
		
		this.LOGGER.info(trace+" "+"adicionales = "+adicionales);
		/*JSONObject monedasDisponibles=this.getDispositivoService().obtenerMonedasSMH(trace);
		this.LOGGER.info(trace+" "+"Monedas disponibles = "+monedasDisponibles);*/

		List<Moneda> listaMonedasDisponibles=new ArrayList<Moneda>();
		JSONObject objtoVuelto = new JSONObject();
		Double montoPendient=0.0;		
		String tramaMonedaDispensadas="";
		/*montoVuelto=5000;
		montoTotalPagad=50000;
		this.costoServicio=45000;*/
		try {
			
			System.out.println("***ENTREGAR VUELTO EN MONEDAS****");
			DispensarMonedas dispensarMonedas=new DispensarMonedas();
			HashMap<String, Object> objectoDispensar=dispensarMonedas.procesarVuelto(trace, monedaPais, montoVuelto, evaCoreService,hooperCantidadMax,cantidadHopper, denominacionHopper, true);
			this.LOGGER.info(trace+" "+"objectoDispensar "+objectoDispensar);
			if(objectoDispensar.get("estado").equals("0")){
				
				
				montoPendient=Double.parseDouble(objectoDispensar.get("montoPendiente").toString());
				//Se agrega para quitar los centimos
				//if(HOPPERACTIVO!=1){
					if(montoPendient==montoVuelto){
						double montoNuevo=Math.floor(montoVuelto);
						if(montoNuevo!=montoVuelto){
							objectoDispensar=dispensarMonedas.procesarVuelto(trace, monedaPais, montoNuevo, evaCoreService ,hooperCantidadMax, cantidadHopper,denominacionHopper, false);
							this.LOGGER.info(trace+" "+"objectoDispensar sin decimal "+objectoDispensar);
							montoPendient=(montoVuelto-montoNuevo) + Double.parseDouble(objectoDispensar.get("montoPendiente").toString());
							this.LOGGER.info(trace+" "+"objectoDispensar -> montoVuelto: "+montoVuelto+"\t montoNuevo: "+montoNuevo+"\t pendiente "+Double.parseDouble(objectoDispensar.get("montoPendiente").toString()));
						}
					}
				//}
				
				HashMap<Double, Integer> monedasDipensadas=(HashMap<Double, Integer>) objectoDispensar.get("dispensado");
				for (Map.Entry monedas : monedasDipensadas.entrySet()) {
					System.out.println("Key: "+monedas.getKey() + " & Value: " + monedas.getValue());
					this.LOGGER.info(trace+" "+"entrega dinero "+"Key: "+monedas.getKey() + " & Value: " + monedas.getValue());
					int denominacion=(int)(Double.parseDouble(monedas.getKey().toString()) * 100);
					//actualizarCantidadSmartHopper(terminal, -1*Integer.parseInt(monedas.getValue().toString()), String.valueOf(denominacion));
					actualizarCantidadMonedasDB("SH01", String.valueOf(denominacion), -1*Integer.parseInt(monedas.getValue().toString()));
				}
				if(objectoDispensar.containsKey("hopper")){
					int cantidadMonedas=Integer.parseInt(objectoDispensar.get("hopper").toString());
					//actualizarCantidadDispositivo(terminal, cantidadMonedas*(-1), String.valueOf(denominacionHopper), "HO01", denominador);
					actualizarCantidadMonedasDB("HO01", String.valueOf(denominacionHopper), cantidadMonedas*(-1));
					//getTransactionLogDAO().registrarVueltoPago(getConn(), trace, terminal, "cTxClientName", denominacionHopper+";"+cantidadMonedas+";HOM01");
					transactionService.actualizarVueltoHopper(trace, nroTerminal, denominacionHopper+";"+cantidadMonedas+";HOM01");
				}
				System.out.println("CADENA DE MONEDAS DISPENSADAS");
				//tramaMonedaDispensadas=formatMonedasSH2(monedasDipensadas);
				tramaMonedaDispensadas = UtilMoneda.getFormatMonedas(monedasDipensadas, deviceService.obtenerDispositivos("SH01"));
				System.out.println(tramaMonedaDispensadas);
			}
			else if(objectoDispensar.get("estado").equals("1")){
				montoPendient=Double.parseDouble(objectoDispensar.get("montoPendiente").toString());
			}
			else if(objectoDispensar.get("estado").equals("-1")){
				montoPendient=Double.parseDouble(objectoDispensar.get("montoPendiente").toString());
			}
			
			//this.getDispositivoService().apagarLuces(trace, Constantes.DISPOSITIVO_HOPPER);
			
		} catch (Exception e) {			
			LOGGER.error(trace+" "+"Error en el método entregar dinero", e);
		}
		
		

		String vueltoBillete = UtilMoneda.getFormatBilletes(new HashMap<Integer, Integer>(),  deviceService.obtenerDispositivos("PY01"));
		
		Double billeteNoDevuelto=0.0;
		Moneda resultBillete = new Moneda();
		List<Moneda> vueltoBilletesEntregados=new ArrayList<Moneda>();
		if(!isCancel || !tipoAceptador.equals("4")){
			if(UtilWeb.VUELTO_BILLETES.size()>0){
				if(tipoAceptador.equals("4")){
					resultBillete=evaCoreService.dispensarBilletes(UtilWeb.VUELTO_BILLETES, trace); //getDispositivoService().dispensarBilletes(vueltoBilletes, trace);
				}
				else{
					resultBillete=evaCoreService.dispensarBilletes(UtilWeb.VUELTO_BILLETES, trace);
					
					if(resultBillete.getEstado()==1){
						vueltoBilletesEntregados=getBilletesDispensados(UtilWeb.VUELTO_BILLETES, resultBillete.getBilletes());
						UtilWeb.VUELTO_BILLETES=new ArrayList<Moneda>(resultBillete.getBilletes());
						
					}
					if(resultBillete.getBilletesCashBox()!=null){						
						HashMap<Integer, Integer> billeteCashBox=resultBillete.getBilletesCashBox();
						LOGGER.info(trace+" ACTUALIZAR MONEDAS EN CASHBOX "+billeteCashBox);
						for (Map.Entry<Integer, Integer> entry : billeteCashBox.entrySet()) {
							Integer key = entry.getKey();
				            Integer value = entry.getValue();
				            if(value>0){
				            	actualizarCantidadMonedasDB("CB01", String.valueOf((key * 100)) , value);
				            	actualizarCantidadMonedasDB("PY01", String.valueOf((key * 100)) , -value);
				            }
						}
					}
				}
				
			
				System.out.println("********RESULT BILLETE******");
				this.LOGGER.info(trace+"********RESULT BILLETE******");
				this.LOGGER.info(resultBillete);
				if(resultBillete==null){
					resultBillete=new Moneda();
					resultBillete.setEstado(-5);
					resultBillete.setDescripcion("Error Desconoccido");
					this.LOGGER.info(trace+" "+"PINTAR ERROR DESCONOCIDO");
				}
				if(resultBillete.getDescripcion().substring(0,4).equals("7D00")){
					resultBillete.setEstado(1);
				}
				if(resultBillete.getDescripcion().substring(0,4).equals("FFE5")){
					resultBillete.setEstado(0);
				}
				
				HashMap<Integer, Integer> hmVueltoBillete=new HashMap<Integer, Integer>();
				for (Moneda moneda : vueltoBilletesEntregados) {
					hmVueltoBillete.put(moneda.getDenominacion(), moneda.getCantidad());
				}
				
				if(resultBillete.getEstado()==1 || resultBillete.getEstado()==2 || resultBillete.getEstado()==-1){
					for (Moneda moneda : UtilWeb.VUELTO_BILLETES) {
						LOGGER.info("billete no entregado -> denominacion = "+moneda.getDenominacion()+" cantidad = "+moneda.getCantidad());
						billeteNoDevuelto=billeteNoDevuelto+(moneda.getDenominacion()*(double) moneda.getCantidad());
						//vueltoBillete=formatBilletesDevueltos(vueltoBilletes);
						//actualizarCantidadDispositivo(nro, moneda.getCantidad(), moneda.getDenominacion() * 100+"", "PY01", denominador);
						actualizarCantidadMonedasDB("PY01", String.valueOf((moneda.getDenominacion() * 100)), moneda.getCantidad());
					}
					//vueltoBillete=formatBilletesDevueltos(vueltoBilletesEntregados);
					
					vueltoBillete=UtilMoneda.getFormatBilletes(hmVueltoBillete, deviceService.obtenerDispositivos("PY01"));
				}
				else if(resultBillete.getEstado()==0){
					hmVueltoBillete.clear();
					for (Moneda moneda : UtilWeb.VUELTO_BILLETES) {
						hmVueltoBillete.put(moneda.getDenominacion(), moneda.getCantidad());
					}
					vueltoBillete = UtilMoneda.getFormatBilletes(hmVueltoBillete, deviceService.obtenerDispositivos("PY01"));
				}
				else{
					vueltoBillete = UtilMoneda.getFormatBilletes(new HashMap<Integer, Integer>(), deviceService.obtenerDispositivos("PY01"));
				}
			}
		}
		
		
		
		List<TmDevice> billetesDevicePayout=deviceService.obtenerDispositivos("PY01");
		this.LOGGER.info(trace+" "+"billeteNoDevuelto = "+billeteNoDevuelto+"\t montoPendient-> "+montoPendient);
		HashMap<String, Object> dataDenominacion = UtilMoneda.obtenerDenominacionIngresada(montoPagadoList);
		if(isCancel){
			try {
				//HashMap<String, Object> dataDenominacion = UtilMoneda.obtenerDenominacionIngresada(montoPagadoList);
				HashMap<Double, Integer> monedas = (HashMap<Double, Integer>) dataDenominacion.get("MONEDAS");
				HashMap<Integer, Integer> billetes = (HashMap<Integer, Integer>) dataDenominacion.get("BILLETESBOX");
				HashMap<Integer, Integer> billetesPayout = (HashMap<Integer, Integer>) dataDenominacion.get("BILLETESPAY");
				HashMap<Integer, Integer> billetesTotal = (HashMap<Integer, Integer>) dataDenominacion.get("BILLETESTOTAL");
				
				//if(billetes.size()>0 || billetesPayout.size()>0){
				if(tipoAceptador.equals("4")){		
				
					this.LOGGER.info(trace+" "+"entro a cfinalizaz scrow");
					//this.getDispositivoService().cancelarScrow(trace);
					evaCoreService.invocarServicioGenerico(trace, Constantes.EVACORE_BILLETERO_CANCELAR_SCROW);
					Thread.sleep(4000);
					//this.getDispositivoService().finalizarScrow(trace, true);
					evaCoreService.finalizarScrow(trace, true);
				}
				
				
			
			
				/*JSONObject json = hiperCenterService.cancelarPago(String.valueOf(totalIngresado), 
						String.valueOf(costoServicio), String.valueOf(totalIngresado), formatBilletesPayout(new HashMap<Integer, Integer>()), "0", "0", trace, 
						getEstadoHopper1(), getEstadoHopper2(), getEstadoHopper3(), 
						trace, "", this.getReadProperty().getString("agencia"), this.getReadProperty().getString("terminal"),
						formatBilletesPayout(new HashMap<Integer, Integer>()), 
						formatBilletesPayout(new HashMap<Integer, Integer>()), "0");*/
				
				
				HashMap<String, String> data=hcService.cancelarPago(String.valueOf(totalIngresado), String.valueOf(montoPagor.getnCostoServicio()), String.valueOf(totalIngresado), UtilMoneda.getFormatBilletes(new HashMap<Integer, Integer>(), deviceService.obtenerDispositivos("PY01")), 
						"0", "0", trace, "", "", "", trace, "", agencia, nroTerminal, 
						UtilMoneda.getFormatBilletes(new HashMap<Integer, Integer>(), billetesDevicePayout),
								UtilMoneda.getFormatBilletes(new HashMap<Integer, Integer>(), billetesDevicePayout), "0");
				
				//this.getTransactionLogDAO().actualizarCancelarMontos(trace, terminal, formatMonedasSH(monedas), String.valueOf(costoServicio), this.getConn());
				transactionService.actualizarCancelarMontos(trace, nroTerminal, UtilMoneda.getFormatMonedas(monedas, deviceService.obtenerDispositivos("SH01")), String.valueOf(montoPagor.getnCostoServicio()));
				//vueltoBillete=formatBilletesCashBox(billetesTotal);
				vueltoBillete=UtilMoneda.getFormatBilletes(billetesTotal, billetesDevicePayout);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(billeteNoDevuelto+montoPendient>0){
			HashMap<Integer, Integer> billetesTotal = (HashMap<Integer, Integer>) dataDenominacion.get("BILLETESTOTAL");
			String billeteND=UtilMoneda.getFormatBilletes(new HashMap<Integer, Integer>(), billetesDevicePayout);//formatBilletesCodigo(new ArrayList<CBDevice>());
			if(billeteNoDevuelto>0){
				billeteND=UtilMoneda.getFormatBilletes(billetesTotal, billetesDevicePayout); //formatBilletesCodigo(vueltoBilleteDispositivo);
			}
			try {
				//HashMap<String, Object> listaDeno=listaDenominaciones(trace, this.getConn());
				HashMap<Double, Integer> monedas = (HashMap<Double, Integer>) dataDenominacion.get("MONEDAS");
				HashMap<Integer, Integer> billetes = (HashMap<Integer, Integer>) dataDenominacion.get("BILLETESBOX");
				HashMap<Integer, Integer> billetesPayout = (HashMap<Integer, Integer>) dataDenominacion.get("BILLETESPAY");
				montoPendiente=new BigDecimal(billeteNoDevuelto+montoPendient).setScale(2, RoundingMode.HALF_UP);
				System.out.println("montoTotalPagad = "+totalIngresado+"\t this.costoServicio = "+montoPagor.getnCostoServicio()+"\t montoPendiente "+montoPendiente);
				BigDecimal montoDevuelto=new BigDecimal((totalIngresado - montoPagor.getnCostoServicio())-montoPendiente.doubleValue()).setScale(2, RoundingMode.HALF_UP);
				if(isCancel){
					montoDevuelto=new BigDecimal(totalIngresado-montoPendiente.doubleValue()).setScale(2, RoundingMode.HALF_UP);
				}
				System.out.println("montoPendiente = "+montoPendiente+"\t montoDevuelto = "+montoDevuelto);
				//JSONObject objetoProcesarDevolucion=procesarDevolucion(trace, "12345678", "producto", UtilWeb.NRO_TICKET, totalIngresado, "", "",montoDevuelto.doubleValue() , montoPendiente.doubleValue(), nroTerminal);
				HashMap<String, String> objetoProcesarDevolucion=hcService.transaccionarDevolucion(trace, agencia,
						"12345678", "producto", UtilWeb.NRO_TICKET, totalIngresado, "",
						"", montoDevuelto.doubleValue(), montoPendiente.doubleValue(), nroTerminal);
				if(objetoProcesarDevolucion.get("estado").equals("0")){
					textoImprimir=objetoProcesarDevolucion.get("print_data");
					//this.getTransactionLogDAO().actualizarprintDataDevolucion(trace, terminal, textoImprimir, "30", this.getConn());
					transactionService.actualizarprintDataDevolucion(trace, nroTerminal, textoImprimir, "30");
				}
				//this.getTransactionLogDAO().actualizarFlagVuelto(trace, terminal, 1, this.getConn());
				transactionService.actualizarFlagVuelto(trace, nroTerminal, 1);
				System.out.println(objetoProcesarDevolucion);
			} catch (Exception e) {
				LOGGER.error(trace+" "+"Error en el método entregar dinero calculo bill", e);
			}
		}
		else{
			try {
				//this.getTransactionLogDAO().actualizarFlagVuelto(trace, terminal, 0, this.getConn());
				transactionService.actualizarFlagVuelto(trace, nroTerminal, 0);
			} catch (Exception e) {
				LOGGER.error(trace+" "+"actualizarFlagVuelto", e);
			}
			
		}
		
		
		montoVuelto=0;
		System.out.println("RESULTADO DEL OBJETO VUELTO MONEDA");
		System.out.println(objtoVuelto);
		System.out.println("RESULTADO DEL OBJETO VUELTO BILLETE");
		System.out.println(resultBillete);
		//MensajeImprimir mensaje=new MensajeImprimir();
		HashMap<String, String> mensaje=new HashMap<String, String>();
		mensaje.put("estado", "0");
		mensaje.put("descripcion", "dispenso ok");
		mensaje.put("textoImprimirVuelto", textoImprimir);
		mensaje.put("textoImprimir", textoImprimir);
		
		DecimalFormat df = new DecimalFormat("#.00");
		String temp = df.format(montoPendiente);
		mensaje.put("vuelto", temp);

		try {
		
			/*getTransactionLogDAO().registrarVueltoPago(getConn(), trace, terminal, "cTxTagsEmv",tramaMonedaDispensadas);
			getTransactionLogDAO().registrarVueltoPago(getConn(), trace, terminal, "cTxAplLabelEmv", vueltoBillete);*/
			transactionService.actualizarVueltoSmartHopper(trace, nroTerminal, tramaMonedaDispensadas);
			transactionService.actualizarVueltoBilletes(trace, nroTerminal, vueltoBillete);
			if(isCancel){
				//getTransactionLogDAO().registrarVueltoPago(getConn(), trace, terminal, "nTxCardNumber3", vueltoBillete);
				transactionService.actualizarVueltoBilletesCancel(trace, nroTerminal, vueltoBillete);
			}
		} catch (Exception e) {
			LOGGER.error(trace+" "+"registrarVueltoPago", e);
		}
		finally{
			try {
				//boolean flagMontoPagado = this.getMontoPagadoDAO().eliminarMontoPagado(this.getConn());
				//boolean flagMontoPagar = this.getMontoPagarDAO().eliminarMontoPagar(this.getConn());
			} catch (Exception e2) {
				LOGGER.error(trace+" "+"eliminarMontoPagado", e2);
			}
			
		}
		
		return mensaje;
	}
	
	public HashMap<String, String> cancelarPago(String trace, String proDescripcion, String codigoVerificacion){
		this.LOGGER.info(trace+" "+"Executing metodo cancelarPago() de Reciclador");
		this.LOGGER.info(trace+" "+"cancelarPago() proDescripcion=" + proDescripcion);
		this.LOGGER.info(trace+" "+"cancelarPago() codigoVerificacion=" + codigoVerificacion);
		//registrarBilletes(trace); aplica para el nv200
		
		
		//setCodigoVerificacionCancelacion(codigoVerificacion);	
	
		this.LOGGER.info(trace+" "+"cancelarPago: Las variables se establecieron en 0");
		int denominador=1;
		if(monedaPais.equals("COP")){
			denominador=100;
		}
		HashMap<String, String> mensaje = new HashMap<String, String>();
		

		this.LOGGER.info(trace+" "+"OBTENIENDO ESTADO DEL SMARTHOPPER");
		
		
		//String tramaSmartHopper = this.getTmDeviceDAO().consultarCantidadDispositivo(getConn(),terminal, "SH01",false);
		List<TmDevice> monedasSH=deviceService.obtenerDispositivos("SH01");
		String tramaSmartHopper=UtilMoneda.getFormatMonedas(new HashMap<Double, Integer>(), monedasSH);
		LOGGER.info(trace+" "+"tramaSmartHopper -> "+cantidadMaximaSH);
		String[] canDenomtSMH=null;
		int cantSmartHopper = monedasSH.stream().mapToInt(d -> Integer.parseInt(d.getnDsQuantityDenomination())).sum();

		LOGGER.info(trace+" "+"cantSmartHopper -> "+cantSmartHopper+" \t umbralSmartHopper: "+cantidadMaximaSH);

		String deviceCode="";
		List<TpMontoPagado> montoPagadoList = montoPagadoService.listarMontoPagado(trace);
		HashMap<String, Object> listaDeno=UtilMoneda.obtenerDenominacionIngresada(montoPagadoList);
		if (cantSmartHopper >= cantidadMaximaSH) {
			// CASHBOX MONEDAS
			LOGGER.info(trace+" "+"ABRIR SCROW2 CASHBOX MONEDAS");
			evaCoreService.devolverScrow(trace);
			HashMap<Double, Integer> monedas = (HashMap<Double, Integer>) listaDeno.get("MONEDAS");
			
			for (Entry<Double, Integer> entry : monedas.entrySet()) {
				int denominacion = (int) (entry.getKey() * 100);
				actualizarCantidadMonedasDB("CB02", String.valueOf(denominacion), entry.getValue());
			}
		} else {
			LOGGER.info(trace+" "+"ABRIR SCROW2 SMARTHOPPER");
			evaCoreService.aceptarScrow(trace);
			HashMap<Double, Integer> monedas = (HashMap<Double, Integer>) listaDeno.get("MONEDAS");
			actualizarCantidadMonedasSM(trace, monedas);
			
		}
		mensaje=entregarDinero(trace, 0, true);
		
		//ACTUALIZANDO CONTADORES 

		return mensaje;
	}

	private String derivarMonedas(String trace) {
		String camino = "1";
		List<TmDevice> monedasSH = deviceService.obtenerDispositivos("SH01");
		int cantidadActualMoneda = monedasSH.stream()
				.mapToInt(device -> Integer.parseInt(device.getnDsQuantityDenomination())).sum();
		System.out.println("cantidad de monedas actuales -> " + cantidadActualMoneda);
		// FALTA IMPLEMENTAR MEDOTO PARA DECIR DONDE SE GUARDAN LAS MONEDAS

		if (cantidadActualMoneda >= cantidadMaximaSH) {
			// implementar this.getDispositivoService().scrowDevolver(trace);
			evaCoreService.devolverScrow(trace);
			camino = "1";
		} else {
			// this.getDispositivoService().scrowAceptar(trace);
			evaCoreService.aceptarScrow(trace);
			camino = "0";
		}

		return camino;
	}

	private void actualizarCantidadMonedasSM(String trace, HashMap<Double, Integer> monedas) {
		System.out.println("actualizarCantidadMonedas -> " + monedas);
		for (Entry<Double, Integer> entry : monedas.entrySet()) {
			// System.out.println("Clave: " + entry.getKey() + ", valor: " +
			// entry.getValue());
			int denominacion = (int) (entry.getKey() * 100);
			evaCoreService.agregarMonedasSH(trace, entry.getValue(), entry.getKey());
			/*TmDevice tmdevice=deviceService.getDenominacionDispositivo(String.valueOf(denominacion), "SH01");
			tmdevice.setnDsQuantityDenomination((Integer.parseInt(tmdevice.getnDsQuantityDenomination())+entry.getValue())+"");
			deviceService.actualizarCantidad(tmdevice);*/
			actualizarCantidadMonedasDB("SH01", String.valueOf(denominacion), entry.getValue());
		}
	}
	
	private void actualizarCantidadMonedasDB(String dispositivo, String denominacion, int cantidad) {
		TmDevice tmdevice=deviceService.getDenominacionDispositivo(String.valueOf(denominacion), dispositivo);
		tmdevice.setnDsQuantityDenomination((Integer.parseInt(tmdevice.getnDsQuantityDenomination())+cantidad)+"");
		//deviceService.actualizarCantidadDenominacion(entry.getValue(), String.valueOf(denominacion), "SH01");
		deviceService.actualizarCantidad(tmdevice);
	}

	public void iniciarAceptadorMonedas(String trace, double costoServicio) {

		if (UtilWeb.TOTAL_MONEDAS_INGRESADAS < cantidadMaximaMonedas) {

			HashMap<String, String> dataIngreso = evaCoreService.serviciosMonedero(trace,
					Constantes.EVACORE_MONEDERO_INICIAR);
			System.out.println("dataIngreso");

			if (dataIngreso.get("estado").equals("1")) {
				taskMonederoPolling = new TaskMonederoPolling(trace, costoServicio);
				if (scheduler.isShutdown()) {
					scheduler = Executors.newScheduledThreadPool(1);
				}
				resultTareaMonedero = scheduler.scheduleAtFixedRate(this.taskMonederoPolling, 500, 500,
						TimeUnit.MILLISECONDS);
			}
		}
	}
	
	private List<Moneda> getBilletesDispensados(List<Moneda> vueltoBillete, List<Moneda> vueltoBillete2){
        List<Moneda> nuevo=new ArrayList<Moneda>();
        int cantidad=0;
        for (Moneda moneda : vueltoBillete) {
            cantidad=moneda.getCantidad();
            for (Moneda moneda2 : vueltoBillete2) {
                if(moneda.getDenominacion()==moneda2.getDenominacion()) {
                    cantidad=moneda.getCantidad()-moneda2.getCantidad();
                    break;
                }
            }
            if(cantidad>0){
               nuevo.add(new Moneda(moneda.getDenominacion(), cantidad)); 
            }

        }

        return nuevo;
    }

	public void detenerAceptadorMonedas() {

		System.out.println("********INGRESO AL METODO DETENERACEPTADORMONEDAS*********");
		System.out.println("");
		if (!resultTareaMonedero.isCancelled()) {
			System.out.println("CANCELAR");
			resultTareaMonedero.cancel(true);
			if (!scheduler.isShutdown())
				System.out.println("DETENER");
			{
				scheduler.shutdown();
			}
		}

	}
	
	private void registrarBilletes(String trace){
		try {			
			if(tipoAceptador.equals("5")){
				Moneda billetesIngresados=evaCoreService.billetesIngresados(trace);
				List<Moneda> billetes=billetesIngresados.getBilletes();
				TpMontoPagar montoPagar = montoPagarService.getMontoPagar(trace);
				TpMontoPagado montoPagado = null;
				for (Moneda billetePay : billetes) {
					
					for (int j = 0; j < billetePay.getCantidadPayout(); j++) {
						/*montoPagado = new MontoPagado();
						montoPagado.setTipo(1);
						montoPagado.setDenominacion(Double.parseDouble(String.valueOf(billetePay.getDenominacion())));
						montoPagado.setTrace(trace);
						montoPagado.setPayout(1);*/
						montoPagado = new TpMontoPagado();
						montoPagado.setnTipo(BILLETES);
						montoPagado.setnDenominacion(Double.parseDouble(String.valueOf(billetePay.getDenominacion())));
						montoPagado.setcTrace(trace);
						montoPagado.setBpayout(1);
						montoPagadoService.saveMontoPagado(montoPagado);
	
						/*boolean grabo = this.getMontoPagadoDAO().grabarMontoPagado(this.getConn(), montoPagado);
						if (grabo) {
							this.LOGGER.info(trace+" "+"Billete de " + billetePay.getDenominacion() + " grabado en BD");
						} else {
							this.LOGGER.info(trace+" "+"No se pudo grabar billete de " + billetePay.getDenominacion());
						}*/
					}
				}
				
				for (Moneda billeteCashBox : billetes) {
					
					for (int k = 0; k < billeteCashBox.getCantidadCashbox(); k++) {
						/*montoPagado = new MontoPagado();
						montoPagado.setTipo(1);
						montoPagado.setDenominacion(Double.parseDouble(String.valueOf(billeteCashBox.getDenominacion())));
						montoPagado.setTrace(trace);
						montoPagado.setPayout(0);
						
						boolean grabo = this.getMontoPagadoDAO().grabarMontoPagado(this.getConn(), montoPagado);
						if (grabo) {
							this.LOGGER.info(trace+" "+"Billete de " + billeteCashBox.getDenominacion() + " grabado en BD");
						} else {
							this.LOGGER.info(trace+" "+"No se pudo grabar billete de " + billeteCashBox.getDenominacion());
						}*/
						montoPagado = new TpMontoPagado();
						montoPagado.setnTipo(BILLETES);
						montoPagado.setnDenominacion(Double.parseDouble(String.valueOf(billeteCashBox.getDenominacion())));
						montoPagado.setcTrace(trace);
						montoPagado.setBpayout(0);
						montoPagadoService.saveMontoPagado(montoPagado);
					}
				}
			
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/* AQUIE EL POOLEO DE monedas */
	private class TaskMonederoPolling extends TimerTask {
		int contador = 0;
		String trace;
		double costo;

		TaskMonederoPolling(String trace, double costo) {
			this.trace = trace;
			this.costo = costo;
		}

		@Override
		public synchronized void run() {
			// TODO Auto-generated method stub
			try {
				HashMap<String, String> dataIngreso = evaCoreService.serviciosMonedero(trace,
						Constantes.EVACORE_MONEDERO_DENOMINACIO);
				System.out.println("contador -> " + contador++);
				System.out.println(dataIngreso);

				if (dataIngreso.get("estado").equals("1") && !dataIngreso.get("descripcion").equals("0.0")) {

					BigDecimal montoIngresado = new BigDecimal(
							UtilWeb.MONTO_INGRESADO + Double.parseDouble(dataIngreso.get("descripcion")));
					montoIngresado = montoIngresado.setScale(2, RoundingMode.HALF_UP);

					UtilWeb.MONTO_INGRESADO = montoIngresado.doubleValue();

					int denominacion = (int) Double.parseDouble(dataIngreso.get("descripcion"));
					TpMontoPagado montoPagado = new TpMontoPagado();
					montoPagado.setnDenominacion(denominacion);
					montoPagado.setcTrace(trace);
					montoPagado.setnTipo(2);
					montoPagadoService.saveMontoPagado(montoPagado);

				}

				if (UtilWeb.MONTO_INGRESADO >= costo) {
					detenerAceptadorMonedas();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}
}
