package pe.com.hiper.service;

import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

import pe.com.hiper.model.Moneda;

public interface EvaCoreService {
	
	public HashMap<String, String> serviciosMonedero(String trace, String servicio);
	
	public Moneda aceptarBilletes(String trace);
	
	public HashMap<String, String> finalizarScrow(String trace, boolean isCancel);
	
	public HashMap<String, String> agregarMonedasSH(String trace, int cantidad, double denominacion);
	
	public HashMap<String, String> aceptarScrow(String trace);
	
	public HashMap<String, String> devolverScrow(String trace);
	
	public HashMap<String, String> invocarServicioGenerico(String trace, String servicio);
	
	public JSONObject obtenerMonedasSH(String trace);
	
	public JSONObject dispensarMonedasSH(String trace, double monto);
	
	public HashMap<String, String> getDispensarHopper(String trace, String direccion, int cantidad);
	
	public Moneda dispensarBilletes(List<Moneda> billetes, String trace);
	
	public Moneda billetesIngresados(String trace);
	

}
