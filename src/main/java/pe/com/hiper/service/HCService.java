package pe.com.hiper.service;

import java.util.HashMap;

import org.json.JSONObject;

public interface HCService {

	public HashMap<String, String> obtenerCostoProducto(String trace, String codigoServicio, String nroCupon);

	public HashMap<String, String> procesarPagoTotalNuevo(double costo_servicio, double monto_pagado,
			String id_producto, String id_cliente, String deno_cant_bill, String deno_cant_mone, String trace,
			String estado_hopper1, String estado_hopper2, String estado_hopper3, String desc_producto, String agencia,
			String terminal, String flagTPP, String flagRecicla, String deno_cant_bill_pay, String camino,
			String numOperacion, String numAutorizacion, String cardNumberEnc, String PaymentTypeCode);

	public HashMap<String, String> pagoPortales(String trace, String terminal, String nroTicket, String nroCupon,
			String fechaIngreso, String fechaSalida, String stacionEntrada, String monto, String impuesto,
			String montoTotal, String tipoPago, String numOperacion, String numAutorizacion, String cardNumberEnc,
			String cardRead, String flagNtaCredito, String montoNtaCredito, String idTrxCastle, String fechaTrxCastle,
			String estadoCastle, String numSerieCastle);

	public HashMap<String, String> voucherPortales(String trace, String terminal, String service_name,
			String payPassBalance, String payPassConsumo, String payPassAmountOld, String payPassCard,
			String payPassName, String payPassDni, String payPassTrace);

	public HashMap<String, String> cancelarPago(String monto_ingresado, String monto_deuda, String monto_retorno,
			String deno_cant_bill, String id_producto, String id_cliente, String trace, String estado_hopper1,
			String estado_hopper2, String estado_hopper3, String num_referencia, String desc_producto, String agencia,
			String terminal, String bill_cancelar_stack, String bill_cancelar_pay, String flagRecicla);

	public HashMap<String, String> transaccionarDevolucion(String trace, String agencia, String id_producto,
			String desc_producto, String id_cliente, double monto_recarga, String bill_val, String mon_val,
			double monto_devuelto, double monto_devolver, String terminal);

	public HashMap<String, String> consultaRucPortales(String trace, String terminal, String ruc, String operationType,
			String name, String street1, String postalCode, String city);
	


}
