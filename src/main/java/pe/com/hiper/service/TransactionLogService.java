package pe.com.hiper.service;


public interface TransactionLogService {

	public void actualizarMonedasIngresada(String trace, String terminal, String formatoMonedas);

	public void actualizarVueltoHopper(String trace, String terminal, String formatoMonedas);

	public void actualizarCancelarMontos(String trace, String terminal, String formatoMoneda, String costoServicio);

	public void actualizarVueltoSmartHopper(String trace, String terminal, String formatoVuelto);

	public void actualizarVueltoBilletes(String trace, String terminal, String formatoVuelto);

	public void actualizarVueltoBilletesCancel(String trace, String terminal, String formatoVuelto);

	public void actualizarprintDataDevolucion(String trace, String terminal, String printData, String tipoTransaccion);

	public void actualizarFlagVuelto(String trace, String terminal, int vuelto);
}
