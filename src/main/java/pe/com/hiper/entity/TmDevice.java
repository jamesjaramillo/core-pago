package pe.com.hiper.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tmdevice")
public class TmDevice implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1726849050864095240L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cdsid")
	private Long cDsId;
	
	@Column(name = "cdsterminal")
	private String cDsTerminal;
	
	@Column(name = "cdsstatusdevice")
	private String cDsStatusDevice;
	
	@Column(name = "cdsdevicecode")
	private String cDsDeviceCode;
	
	@Column(name = "ddsdevicename")
	private String dDsDeviceName;
	
	@Column(name = "cdsmoneycode")
	private String cDsMoneyCode;
	
	@Column(name = "cdscodedenomination")
	private String cDsCodeDenomination;
	
	@Column(name = "cdsinidenomination")
	private String cDsIniDenomination;
	
	@Column(name = "ndsquantitydenomination")
	private String nDsQuantityDenomination;
	
	@Column(name = "cdslogicalstatus")
	private String cDsLogicalStatus;
	
	@Column(name = "fdsdateinidenomination")
	private Date fDsDateIniDenomination;
	
	@Column(name = "fdsdatequantitydenomination")
	private Date fDsDateQuantityDenomination;

	public Long getcDsId() {
		return cDsId;
	}

	public void setcDsId(Long cDsId) {
		this.cDsId = cDsId;
	}

	public String getcDsTerminal() {
		return cDsTerminal;
	}

	public void setcDsTerminal(String cDsTerminal) {
		this.cDsTerminal = cDsTerminal;
	}

	public String getcDsStatusDevice() {
		return cDsStatusDevice;
	}

	public void setcDsStatusDevice(String cDsStatusDevice) {
		this.cDsStatusDevice = cDsStatusDevice;
	}

	public String getcDsDeviceCode() {
		return cDsDeviceCode;
	}

	public void setcDsDeviceCode(String cDsDeviceCode) {
		this.cDsDeviceCode = cDsDeviceCode;
	}

	public String getdDsDeviceName() {
		return dDsDeviceName;
	}

	public void setdDsDeviceName(String dDsDeviceName) {
		this.dDsDeviceName = dDsDeviceName;
	}

	public String getcDsMoneyCode() {
		return cDsMoneyCode;
	}

	public void setcDsMoneyCode(String cDsMoneyCode) {
		this.cDsMoneyCode = cDsMoneyCode;
	}

	public String getcDsCodeDenomination() {
		return cDsCodeDenomination;
	}

	public void setcDsCodeDenomination(String cDsCodeDenomination) {
		this.cDsCodeDenomination = cDsCodeDenomination;
	}

	public String getcDsIniDenomination() {
		return cDsIniDenomination;
	}

	public void setcDsIniDenomination(String cDsIniDenomination) {
		this.cDsIniDenomination = cDsIniDenomination;
	}

	public String getnDsQuantityDenomination() {
		return nDsQuantityDenomination;
	}

	public void setnDsQuantityDenomination(String nDsQuantityDenomination) {
		this.nDsQuantityDenomination = nDsQuantityDenomination;
	}

	public String getcDsLogicalStatus() {
		return cDsLogicalStatus;
	}

	public void setcDsLogicalStatus(String cDsLogicalStatus) {
		this.cDsLogicalStatus = cDsLogicalStatus;
	}

	public Date getfDsDateIniDenomination() {
		return fDsDateIniDenomination;
	}

	public void setfDsDateIniDenomination(Date fDsDateIniDenomination) {
		this.fDsDateIniDenomination = fDsDateIniDenomination;
	}

	public Date getfDsDateQuantityDenomination() {
		return fDsDateQuantityDenomination;
	}

	public void setfDsDateQuantityDenomination(Date fDsDateQuantityDenomination) {
		this.fDsDateQuantityDenomination = fDsDateQuantityDenomination;
	}

	

}
