package pe.com.hiper.repository;

import java.sql.Connection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import pe.com.hiper.entity.TpTransactionLog;

@Repository
public interface TransactionLogRepository extends JpaRepository<TpTransactionLog, Long> {

	@Transactional
	@Modifying
	@Query(value = "UPDATE tpTransactionLog set nTxCardNumber2=:formatoVuelto WHERE cTxTerminalSerial=:terminal AND cTxTxnNumber=:trace AND (cTxType='21' or cTxType='30' or cTxType='08')", nativeQuery = true)
	public void actualizarMonedasIngresada(@Param("trace") String trace, @Param("terminal") String terminal,
			@Param("formatoVuelto") String formatoVuelto);

	@Transactional
	@Modifying
	@Query(value = "UPDATE tpTransactionLog set cTxClientName=:formatoVuelto WHERE cTxTerminalSerial=:terminal AND cTxTxnNumber=:trace AND (cTxType='21' or cTxType='30' or cTxType='08')", nativeQuery = true)
	public void actualizarVueltoHopper(@Param("trace") String trace, @Param("terminal") String terminal,
			@Param("formatoVuelto") String formatoVuelto);

	@Transactional
	@Modifying
	@Query(value = "update tpTransactionLog set nTxAmount=:costoServicio , nTxCardNumber2=:formatoMoneda where cTxTxnNumber=:trace and cTxTerminalSerial=:terminal and ctxtype='08'", nativeQuery = true)
	public void actualizarCancelarMontos(@Param("trace") String trace, @Param("terminal") String terminal,
			@Param("formatoMoneda") String formatoMoneda, @Param("costoServicio") String costoServicio);

	@Transactional
	@Modifying
	@Query(value = "UPDATE tpTransactionLog set cTxTagsEmv=:formatoVuelto WHERE cTxTerminalSerial=:terminal AND cTxTxnNumber=:trace AND (cTxType='21' or cTxType='30' or cTxType='08')", nativeQuery = true)
	public void actualizarVueltoSmartHopper(@Param("trace") String trace, @Param("terminal") String terminal,
			@Param("formatoVuelto") String formatoVuelto);

	@Transactional
	@Modifying
	@Query(value = "UPDATE tpTransactionLog set cTxAplLabelEmv=:formatoVuelto WHERE cTxTerminalSerial=:terminal AND cTxTxnNumber=:trace AND (cTxType='21' or cTxType='30' or cTxType='08')", nativeQuery = true)
	public void actualizarVueltoBilletes(@Param("trace") String trace, @Param("terminal") String terminal,
			@Param("formatoVuelto") String formatoVuelto);

	@Transactional
	@Modifying
	@Query(value = "UPDATE tpTransactionLog set nTxCardNumber3=:formatoVuelto WHERE cTxTerminalSerial=:terminal AND cTxTxnNumber=:trace AND (cTxType='21' or cTxType='30' or cTxType='08')", nativeQuery = true)
	public void actualizarVueltoBilletesCancel(@Param("trace") String trace, @Param("terminal") String terminal,
			@Param("formatoVuelto") String formatoVuelto);

	@Transactional
	@Modifying
	@Query(value = "update tpTransactionLog set cTxPrintData=:printData where cTxTxnNumber=:trace and cTxTerminalSerial=:terminal and ctxtype=:tipoTransaccion", nativeQuery = true)
	public void actualizarprintDataDevolucion(@Param("trace") String trace, @Param("terminal") String terminal,
			@Param("printData") String printData, @Param("tipoTransaccion") String tipoTransaccion);
	
	@Transactional
	@Modifying
	@Query(value = "update tpTransactionLog set ctxCompany=:vuelto where cTxTxnNumber=:trace and cTxTerminalSerial=:terminal and ctxtype='21'", nativeQuery = true)
	public void actualizarFlagVuelto(@Param("trace") String trace, @Param("terminal") String terminal, @Param("vuelto") int vuelto);
}
