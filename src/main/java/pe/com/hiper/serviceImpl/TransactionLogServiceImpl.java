package pe.com.hiper.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.hiper.repository.TransactionLogRepository;
import pe.com.hiper.service.TransactionLogService;

@Service
public class TransactionLogServiceImpl implements TransactionLogService {

	@Autowired
	TransactionLogRepository transactionLogRepo;
	
	@Override
	public void actualizarMonedasIngresada(String trace, String terminal, String formatoMonedas) {
		// TODO Auto-generated method stub
		System.out.println("actualizarMonedasIngresada -> "+formatoMonedas);
		
		transactionLogRepo.actualizarMonedasIngresada(trace, terminal, formatoMonedas);
	}

	@Override
	public void actualizarVueltoHopper(String trace, String terminal, String formatoMonedas) {
		// TODO Auto-generated method stub
		transactionLogRepo.actualizarVueltoHopper(trace, terminal, formatoMonedas);
	}

	@Override
	public void actualizarCancelarMontos(String trace, String terminal, String formatoMoneda, String costoServicio) {
		// TODO Auto-generated method stub
		transactionLogRepo.actualizarCancelarMontos(trace, terminal, formatoMoneda, costoServicio);
	}

	@Override
	public void actualizarVueltoSmartHopper(String trace, String terminal, String formatoVuelto) {
		// TODO Auto-generated method stub
		transactionLogRepo.actualizarVueltoSmartHopper(trace, terminal, formatoVuelto);
		
	}

	@Override
	public void actualizarVueltoBilletes(String trace, String terminal, String formatoVuelto) {
		// TODO Auto-generated method stub
		transactionLogRepo.actualizarVueltoBilletes(trace, terminal, formatoVuelto);
		
	}

	@Override
	public void actualizarVueltoBilletesCancel(String trace, String terminal, String formatoVuelto) {
		// TODO Auto-generated method stub
		transactionLogRepo.actualizarVueltoBilletesCancel(trace, terminal, formatoVuelto);
				
	}

	@Override
	public void actualizarprintDataDevolucion(String trace, String terminal, String printData, String tipoTransaccion) {
		// TODO Auto-generated method stub
		transactionLogRepo.actualizarprintDataDevolucion(trace, terminal, printData, tipoTransaccion);
		
	}

	@Override
	public void actualizarFlagVuelto(String trace, String terminal, int vuelto) {
		// TODO Auto-generated method stub
		transactionLogRepo.actualizarFlagVuelto(trace, terminal, vuelto);
	}
	
}
