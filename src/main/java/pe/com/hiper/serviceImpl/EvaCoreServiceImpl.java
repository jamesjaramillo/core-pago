package pe.com.hiper.serviceImpl;

import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import pe.com.hiper.common.Constantes;
import pe.com.hiper.common.UtilWeb;
import pe.com.hiper.entity.TpComponenteLog;
import pe.com.hiper.model.Moneda;
import pe.com.hiper.service.EvaCoreService;
import pe.com.hiper.service.ComponenteLogService;

@Service
public class EvaCoreServiceImpl implements EvaCoreService {
	private static final Logger LOGGER = LogManager.getLogger(EvaCoreServiceImpl.class);
	
	@Autowired
	private ComponenteLogService componenteService;
	
	@Value("${kiosco.terminal}")
	private String nroTerminal;
	
	@Value("${url.api.evaCore}")
	private String urlApi;

	@Override
	public HashMap<String, String> serviciosMonedero(String trace, String servicio) {
		LOGGER.info(trace + " INICIO serviciosMonedero");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "serviciosMonedero");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_API_CORE);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();
			//restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("rrondinel", "1234"));
			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT serviciosMonedero " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi + servicio,
					request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT serviciosMonedero " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR serviciosMonedero", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public Moneda aceptarBilletes(String trace) {
		LOGGER.info(trace + " INICIO aceptarBilletes");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "aceptarBilletes");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_API_CORE);
		Moneda moneda = new Moneda();
		try {
			RestTemplate restTemplate = new RestTemplate();
			//restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("rrondinel", "1234"));
			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT aceptarBilletes " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<Moneda> result = restTemplate.postForEntity(urlApi + Constantes.EVACORE_BILLETERO_ACEPTAR,
					request, Moneda.class);
			moneda = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT aceptarBilletes " + result.toString());
		} catch (Exception e) {
			moneda.setEstado(-1);
			moneda.setDescripcion("Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR aceptarBilletes", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return moneda;
	}

	@Override
	public HashMap<String, String> finalizarScrow(String trace, boolean isCancel) {
		LOGGER.info(trace + " INICIO finalizarScrow");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "finalizarScrow");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_API_CORE);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();
			//restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("rrondinel", "1234"));
			JSONObject input = new JSONObject();
			input.put("trace", trace);
			input.put("isCancel", isCancel);
			LOGGER.info(trace + " INPUT finalizarScrow " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi + Constantes.EVACORE_BILLETERO_FINALIZARSCROW,
					request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT finalizarScrow " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR finalizarScrow", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> agregarMonedasSH(String trace, int cantidad, double denominacion) {
		LOGGER.info(trace + " INICIO agregarMonedasSH");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "agregarMonedasSH");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_API_CORE);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();
			//restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("rrondinel", "1234"));
			JSONObject input = new JSONObject();
			input.put("trace", trace);
			input.put("cantidad", cantidad);
			input.put("denominacion", denominacion);
			LOGGER.info(trace + " INPUT agregarMonedasSH " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi + Constantes.EVACORE_SMARTH_AGREGAR,
					request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT agregarMonedasSH " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR agregarMonedasSH", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}
	
	@Override
	public HashMap<String, String> aceptarScrow(String trace) {
		LOGGER.info(trace + " INICIO aceptarScrow");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "aceptarScrow");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_API_CORE);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();
			//restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("rrondinel", "1234"));
			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT aceptarScrow " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi + Constantes.EVACORE_ESCROW_ACEPTAR,
					request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT aceptarScrow " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR aceptarScrow", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> devolverScrow(String trace) {
		// TODO Auto-generated method stub
		LOGGER.info(trace + " INICIO devolverScrow");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "devolverScrow");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_API_CORE);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();
			//restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("rrondinel", "1234"));
			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT devolverScrow " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi + Constantes.EVACORE_ESCROW_DEVOLVER,
					request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT devolverScrow " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR devolverScrow", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> invocarServicioGenerico(String trace, String servicio) {
		// TODO Auto-generated method stub
		LOGGER.info(trace + " INICIO invocarServicioGenerico");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, servicio);
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_API_CORE);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();
			//restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("rrondinel", "1234"));
			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT invocarServicioGenerico " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi + servicio,
					request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT invocarServicioGenerico " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR invocarServicioGenerico", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public JSONObject dispensarMonedasSH(String trace, double monto) {
		// TODO Auto-generated method stub
		LOGGER.info(trace + " INICIO dispensarMonedasSH");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "dispensarMonedasSH");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_API_CORE);
		JSONObject data = null;
		try {
			RestTemplate restTemplate = new RestTemplate();
			//restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("rrondinel", "1234"));
			JSONObject input = new JSONObject();
			input.put("trace", trace);
			input.put("monto", monto);
			LOGGER.info(trace + " INPUT dispensarMonedasSH " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<String> result = restTemplate.postForEntity(urlApi + Constantes.EVACORE_SMARTH_DISPENSAR,
					request, String.class);
			data = new JSONObject(result.getBody());
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT dispensarMonedasSH " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR dispensarMonedasSH", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> getDispensarHopper(String trace, String direccion, int cantidad) {
		// TODO Auto-generated method stub
		LOGGER.info(trace + " INICIO getDispensarHopper");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "getDispensarHopper");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_API_CORE);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();
			//restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("rrondinel", "1234"));
			JSONObject input = new JSONObject();
			input.put("trace", trace);
			input.put("direccion", direccion);
			input.put("cantidad", cantidad);
			LOGGER.info(trace + " INPUT getDispensarHopper " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi + Constantes.EVACORE_HOPPER_DISPENSAR,
					request, HashMap.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT getDispensarHopper " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR getDispensarHopper", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public JSONObject obtenerMonedasSH(String trace) {
		// TODO Auto-generated method stub
		LOGGER.info(trace + " INICIO obtenerMonedasSH");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "obtenerMonedasSH");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_API_CORE);
		//HashMap<String, Object> data = new HashMap<String, Object>();
		JSONObject  data=null;
		try {
			RestTemplate restTemplate = new RestTemplate();
			//restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("rrondinel", "1234"));
			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT obtenerMonedasSH " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<String> result = restTemplate.postForEntity(urlApi + Constantes.EVACORE_SMARTH_OBTENER_MON,
					request, String.class);
			data = new JSONObject(result.getBody());
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT obtenerMonedasSH " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR obtenerMonedasSH", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public Moneda dispensarBilletes(List<Moneda> billetes, String trace) {
		LOGGER.info(trace + " INICIO dispensarBilletes");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "dispensarBilletes");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_API_CORE);
		//HashMap<String, Object> data = new HashMap<String, Object>();
		Moneda  data=null;
		try {
			RestTemplate restTemplate = new RestTemplate();
			//restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("rrondinel", "1234"));
			JSONObject input = new JSONObject();
			input.put("trace", trace);
			input.put("billetes", billetes);
			LOGGER.info(trace + " INPUT dispensarBilletes " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<Moneda> result = restTemplate.postForEntity(urlApi + Constantes.EVACORE_BILLETERO_DISPENSAR,
					request, Moneda.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT dispensarBilletes " + result.toString());
		} catch (Exception e) {			
			data.setEstado(-1);
			data.setDescripcion("Error de conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR dispensarBilletes", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public Moneda billetesIngresados(String trace) {
		LOGGER.info(trace + " INICIO billetesIngresados");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "billetesIngresados");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_API_CORE);
		//HashMap<String, Object> data = new HashMap<String, Object>();
		Moneda  data=null;
		try {
			RestTemplate restTemplate = new RestTemplate();
			//restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor("rrondinel", "1234"));
			JSONObject input = new JSONObject();
			input.put("trace", trace);
			LOGGER.info(trace + " INPUT billetesIngresados " + input.toString());
			HttpEntity<String> request = new HttpEntity<String>(input.toString(), UtilWeb.getHeader());
			ResponseEntity<Moneda> result = restTemplate.postForEntity(urlApi + Constantes.EVACORE_BILLETERO_BILLETES_INGRE,
					request, Moneda.class);
			data = result.getBody();
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT billetesIngresados " + result.toString());
		} catch (Exception e) {			
			data.setEstado(-1);
			data.setDescripcion("Error de conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR billetesIngresados", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

}
