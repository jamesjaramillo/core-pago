package pe.com.hiper.serviceImpl;

import java.text.DecimalFormat;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.hiper.common.UtilWeb;
import pe.com.hiper.entity.TpComponenteLog;
import pe.com.hiper.service.ComponenteLogService;
import pe.com.hiper.service.HCService;

/**
 * 
 * @author rrondinel
 *
 */

@Service
public class HCServiceImpl implements HCService {
	private static final Logger LOGGER = LogManager.getLogger(HCServiceImpl.class);
	DecimalFormat formatter = new DecimalFormat("#0.00");

	@Value("${kiosco.terminal}")
	private String nroTerminal;

	@Value("${url.api.hc}")
	private String urlApi;

	@Value("${hcenter.aplicacion}")
	private String aplHcenter;

	@Autowired
	private ComponenteLogService componenteService;

	@Override
	public HashMap<String, String> obtenerCostoProducto(String trace, String codigoServicio, String nroCupon) {
		LOGGER.info(trace + " INICIO obtenerCostoProducto");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "obtenerCostoProducto");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_HIPERCENTER);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject jsonRequest = new JSONObject();
			jsonRequest.put("transaction_code", "TRX_DEUDA_PORTALES");
			jsonRequest.put("aplicacion_trx", aplHcenter);
			jsonRequest.put("numero_ticket", codigoServicio);
			jsonRequest.put("codigoCupon", nroCupon);
			jsonRequest.put("data", "transaction_code;aplicacion_trx;numero_ticket;codigoCupon");
			jsonRequest.put("terminal_serial_number", nroTerminal);
			jsonRequest.put("trace_number", trace);

			LOGGER.info(trace + " INPUT obtenerCostoProducto " + jsonRequest.toString());
			HttpEntity<String> request = new HttpEntity<String>(jsonRequest.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi, request, HashMap.class);
			data = result.getBody();
			if (!data.isEmpty()) {
				data.put("estado", "0");
			} else {
				data.put("estado", "1");
			}
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT obtenerCostoProducto " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR obtenerCostoProducto", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> procesarPagoTotalNuevo(double costo_servicio, double monto_pagado,
			String id_producto, String id_cliente, String deno_cant_bill, String deno_cant_mone, String trace,
			String estado_hopper1, String estado_hopper2, String estado_hopper3, String desc_producto, String agencia,
			String terminal, String flagTPP, String flagRecicla, String deno_cant_bill_pay, String camino,
			String numOperacion, String numAutorizacion, String cardNumberEnc, String PaymentTypeCode) {
		LOGGER.info(trace + " INICIO procesarPagoTotalNuevo");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "procesarPagoTotalNuevo");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_HIPERCENTER);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject jsonRequest = new JSONObject();
			jsonRequest.put("transaction_code", "TRX_PAGO_TOTAL");
//			jsonRequest.put("aplicacion_trx","SWITCH");
			jsonRequest.put("aplicacion_trx", aplHcenter);
			jsonRequest.put("service_code", id_producto);
			jsonRequest.put("desc_service", desc_producto);
			jsonRequest.put("monto_ingresado", formatter.format(monto_pagado).replace(",", "."));
			jsonRequest.put("monto_maximo", formatter.format(costo_servicio).replace(",", "."));
			jsonRequest.put("calculaVueltoHC", "0");
			jsonRequest.put("agencia", agencia);
			jsonRequest.put("cashbox", deno_cant_bill);
			jsonRequest.put("payout", deno_cant_bill_pay);
			jsonRequest.put("smartHopper", deno_cant_mone);
			jsonRequest.put("cod_user", id_cliente);
			jsonRequest.put("estado_hopper1", estado_hopper1);
			jsonRequest.put("estado_hopper2", estado_hopper2);
			jsonRequest.put("estado_hopper3", estado_hopper3);
			jsonRequest.put("cabecera_voucher", "");// upc
			jsonRequest.put("desc_servicio_voucher", "PRODUCTO");
			jsonRequest.put("cliente_voucher", "CLIENTE");
			jsonRequest.put("desc_costo_voucher", "TOTAL A PAGAR");
			jsonRequest.put("desc_pago_voucher", "MONTO PAGADO");
			jsonRequest.put("num_tarjeta", id_cliente);
			jsonRequest.put("num_tarjeta_orig", "1234565678");
			jsonRequest.put("monto_recarga", formatter.format(monto_pagado).replace(",", "."));
			jsonRequest.put("flagTPP", flagTPP);
			jsonRequest.put("flagRecicla", flagRecicla);
			jsonRequest.put("flag_cambio", camino);

			if (numOperacion.equals("") && numAutorizacion.equals("")) {
				jsonRequest.put("data",
						"transaction_code;aplicacion_trx;service_code;desc_service;monto_ingresado;monto_maximo;agencia;cashbox;payout;smartHopper;cod_user;estado_hopper1;estado_hopper2;estado_hopper3;cabecera_voucher;desc_servicio_voucher;cliente_voucher;desc_costo_voucher;desc_pago_voucher;num_tarjeta;num_tarjeta_orig;monto_recarga;flagTPP;flagRecicla;calculaVueltoHC;flag_cambio");
			} else {
				jsonRequest.put("numOperacion", numOperacion);
				jsonRequest.put("numAutorizacion", numAutorizacion);
				jsonRequest.put("cardNumberEnc", cardNumberEnc);
				jsonRequest.put("PaymentTypeCode", PaymentTypeCode);

				jsonRequest.put("data",
						"transaction_code;aplicacion_trx;service_code;desc_service;monto_ingresado;monto_maximo;agencia;cashbox;payout;smartHopper;cod_user;estado_hopper1;estado_hopper2;estado_hopper3;cabecera_voucher;desc_servicio_voucher;cliente_voucher;desc_costo_voucher;desc_pago_voucher;num_tarjeta;num_tarjeta_orig;monto_recarga;flagTPP;flagRecicla;calculaVueltoHC;flag_cambio;numOperacion;numAutorizacion;cardNumberEnc;PaymentTypeCode");
			}

			jsonRequest.put("terminal_serial_number", terminal);
			jsonRequest.put("trace_number", trace);

			LOGGER.info(trace + " INPUT procesarPagoTotalNuevo " + jsonRequest.toString());
			HttpEntity<String> request = new HttpEntity<String>(jsonRequest.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi, request, HashMap.class);
			data = result.getBody();
			if (!data.isEmpty()) {
				data.put("estado", "0");
			} else {
				data.put("estado", "-1");
			}
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT procesarPagoTotalNuevo " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR procesarPagoTotalNuevo", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> pagoPortales(String trace, String terminal, String nroTicket, String nroCupon,
			String fechaIngreso, String fechaSalida, String stacionEntrada, String monto, String impuesto,
			String montoTotal, String tipoPago, String numOperacion, String numAutorizacion, String cardNumberEnc,
			String cardRead, String flagNtaCredito, String montoNtaCredito, String idTrxCastle, String fechaTrxCastle,
			String estadoCastle, String numSerieCastle) {
		// TODO Auto-generated method stub
		LOGGER.info(trace + " INICIO pagoPortales");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "pagoPortales");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_HIPERCENTER);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject jsonRequest = new JSONObject();
			jsonRequest.put("transaction_code", "TRX_PAGO_PORTALES");
			jsonRequest.put("aplicacion_trx", aplHcenter);
			jsonRequest.put("fecha", fechaIngreso);
			jsonRequest.put("fechaSalida", fechaSalida);
			jsonRequest.put("numero_ticket", nroTicket);
			jsonRequest.put("estacionEntrada", stacionEntrada);
			jsonRequest.put("monto", monto);
			jsonRequest.put("impuesto", impuesto);
			jsonRequest.put("monto_ingresado", montoTotal);
			jsonRequest.put("PaymentTypeCode", tipoPago);// 0=factura, 1=recibo, 3=cancelacion

			// 1-si nota credito 0-no hay nota credito
			jsonRequest.put("credito", flagNtaCredito);
			jsonRequest.put("cash_back", montoNtaCredito);

			if (numOperacion.equals("") && numAutorizacion.equals("")) {
				jsonRequest.put("data",
						"transaction_code;aplicacion_trx;fecha;fechaSalida;numero_ticket;estacionEntrada;monto;impuesto;monto_ingresado;PaymentTypeCode;credito;cash_back");
			} else {
				jsonRequest.put("payment_method", "1");

				jsonRequest.put("card_read", cardRead);
				jsonRequest.put("numAutorizacion", numAutorizacion);
				jsonRequest.put("numOperacion", numOperacion);
				jsonRequest.put("cardNumberEnc", cardNumberEnc);

				jsonRequest.put("payPassTrace", idTrxCastle);
				jsonRequest.put("exp_date", fechaTrxCastle);
				jsonRequest.put("visaCheckOut", estadoCastle);
				jsonRequest.put("nro_serie_ppad", numSerieCastle);

				jsonRequest.put("data",
						"transaction_code;aplicacion_trx;fecha;fechaSalida;numero_ticket;estacionEntrada;monto;impuesto;monto_ingresado;PaymentTypeCode;payment_method;numOperacion;numAutorizacion;cardNumberEnc;card_read;payPassTrace;exp_date;visaCheckOut;nro_serie_ppad;credito;cash_back");
			}

			jsonRequest.put("terminal_serial_number", terminal);
			jsonRequest.put("trace_number", trace);

			LOGGER.info(trace + " INPUT pagoPortales " + jsonRequest.toString());
			HttpEntity<String> request = new HttpEntity<String>(jsonRequest.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi, request, HashMap.class);
			data = result.getBody();
			if (!data.isEmpty()) {
				data.put("estado", "0");
			} else {
				data.put("estado", "-1");
			}
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT pagoPortales " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR pagoPortales", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> voucherPortales(String trace, String terminal, String service_name,
			String payPassBalance, String payPassConsumo, String payPassAmountOld, String payPassCard,
			String payPassName, String payPassDni, String payPassTrace) {
		LOGGER.info(trace + " INICIO voucherPortales");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "voucherPortales");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_HIPERCENTER);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();

			JSONObject jsonRequest = new JSONObject();
			jsonRequest.put("transaction_code", "TRX_VOUCHER_PORTALES");
			jsonRequest.put("aplicacion_trx", aplHcenter);
			jsonRequest.put("service_name", service_name);

			if (service_name.equals("0")) {
				jsonRequest.put("data", "transaction_code;aplicacion_trx");
			} else {
				jsonRequest.put("payPassBalance", payPassBalance);
				jsonRequest.put("payPassConsumo", payPassConsumo);
				jsonRequest.put("payPassAmountOld", payPassAmountOld);
				jsonRequest.put("payPassCard", payPassCard);
				jsonRequest.put("payPassName", payPassName);
				jsonRequest.put("payPassDni", payPassDni);
				jsonRequest.put("payPassTrace", payPassTrace);
				jsonRequest.put("data",
						"transaction_code;aplicacion_trx;service_name;payPassBalance;payPassConsumo;payPassAmountOld;payPassCard;payPassName;payPassDni;payPassTrace");
			}

			jsonRequest.put("terminal_serial_number", terminal);
			jsonRequest.put("trace_number", trace);

			LOGGER.info(trace + " INPUT voucherPortales " + jsonRequest.toString());
			HttpEntity<String> request = new HttpEntity<String>(jsonRequest.toString(), UtilWeb.getHeader());
			ResponseEntity<String> result = restTemplate.postForEntity(urlApi, request, String.class);
			LOGGER.info(trace + " " + "RESPONSE=" + result);
			String sValRetornoHCenter = result.getBody();
			String dataImprimir = sValRetornoHCenter.replace("\":\"", "@:@");
			dataImprimir = dataImprimir.replace("\",\"", "@,@");
			dataImprimir = dataImprimir.replace("{\"", "@{@");
			dataImprimir = dataImprimir.replace("\"}", "@}@");
			dataImprimir = dataImprimir.replace("\"", "\\\"");

			dataImprimir = dataImprimir.replace("@}@", "\"}");
			dataImprimir = dataImprimir.replace("@{@", "{\"");
			dataImprimir = dataImprimir.replace("@,@", "\",\"");
			dataImprimir = dataImprimir.replace("@:@", "\":\"");
			sValRetornoHCenter = dataImprimir;
			LOGGER.info(trace + " " + "RESPONSE corregido=" + sValRetornoHCenter);

			JSONObject dataJson = new JSONObject(sValRetornoHCenter);

			data = new ObjectMapper().readValue(dataJson.toString(), HashMap.class);
				
			if (!data.isEmpty()) {
				data.put("estado", "0");
			} else {
				data.put("estado", "-1");
			}
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT voucherPortales " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR voucherPortales", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> cancelarPago(String monto_ingresado, String monto_deuda, String monto_retorno,
			String deno_cant_bill, String id_producto, String id_cliente, String trace, String estado_hopper1,
			String estado_hopper2, String estado_hopper3, String num_referencia, String desc_producto, String agencia,
			String terminal, String bill_cancelar_stack, String bill_cancelar_pay, String flagRecicla) {
		// TODO Auto-generated method stub
		LOGGER.info(trace + " INICIO cancelarPago");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "cancelarPago");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_HIPERCENTER);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();
			JSONObject jsonRequest = new JSONObject();
			
			jsonRequest.put("transaction_code","CANCELAR_PAGO");
//			jsonRequest.put("aplicacion_trx","SWITCH");
			jsonRequest.put("aplicacion_trx", aplHcenter);
			jsonRequest.put("num_referencia",num_referencia);
			jsonRequest.put("service_code",id_producto);
			jsonRequest.put("agencia",agencia);
			jsonRequest.put("desc_service",desc_producto);
			jsonRequest.put("cabecera_voucher","CIBERTEC");
			jsonRequest.put("desc_servicio_voucher","MENSUALIDAD");
			jsonRequest.put("cliente_voucher","CODIGO ALUMNO");
			jsonRequest.put("desc_pago_voucher","MONTO DEVUELTO");
			jsonRequest.put("monto_ingresado",(formatter.format(Double.parseDouble(monto_ingresado))).replace(",", "."));
			jsonRequest.put("monto_deuda",(formatter.format(Double.parseDouble(monto_deuda) )).replace(",", "."));
			jsonRequest.put("monto_retorno",(formatter.format(Double.parseDouble(monto_retorno))).replace(",", "."));
			jsonRequest.put("bill_val",deno_cant_bill);
			jsonRequest.put("bill_cancelar_stack",bill_cancelar_stack);
			jsonRequest.put("bill_cancelar_pay",bill_cancelar_pay);
			jsonRequest.put("flagRecicla",flagRecicla);
			jsonRequest.put("estado_hopper1",estado_hopper1);
			jsonRequest.put("estado_hopper2",estado_hopper2);
			jsonRequest.put("estado_hopper3",estado_hopper3);
			jsonRequest.put("cod_user",id_cliente);
			jsonRequest.put("data","transaction_code;aplicacion_trx;num_referencia;service_code;agencia;desc_service;cabecera_voucher;desc_servicio_voucher;cliente_voucher;desc_pago_voucher;monto_ingresado;monto_deuda;monto_retorno;bill_val;estado_hopper1;estado_hopper2;estado_hopper3;cod_user;flagRecicla;bill_cancelar_stack;bill_cancelar_pay");
			jsonRequest.put("terminal_serial_number",terminal);
			jsonRequest.put("trace_number",trace);

			LOGGER.info(trace + " INPUT cancelarPago " + jsonRequest.toString());
			HttpEntity<String> request = new HttpEntity<String>(jsonRequest.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi, request, HashMap.class);
			data = result.getBody();
			if (!data.isEmpty()) {
				data.put("estado", "0");
			} else {
				data.put("estado", "-1");
			}
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT cancelarPago " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR cancelarPago", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> transaccionarDevolucion(String trace, String agencia, String id_producto, String desc_producto,
			String id_cliente, double monto_recarga, String bill_val, String mon_val, double monto_devuelto,
			double monto_devolver, String terminal) {
		// TODO Auto-generated method stub
		LOGGER.info(trace + " INICIO cancelarPago");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "cancelarPago");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_HIPERCENTER);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();
			JSONObject jsonRequest = new JSONObject();
			
			jsonRequest.put("transaction_code", "DEVUELVE_BILLETE");
//			jsonRequest.put("aplicacion_trx","SWITCH");
			jsonRequest.put("aplicacion_trx", aplHcenter);
			jsonRequest.put("service_code",id_producto);
			jsonRequest.put("desc_service",desc_producto);
			jsonRequest.put("cod_user",id_cliente);
			jsonRequest.put("agencia",agencia);
			jsonRequest.put("num_referencia",trace);
//			jsonRequest.put("num_tarjeta", num_tarjeta);
			jsonRequest.put("num_tarjeta", id_cliente);
			jsonRequest.put("num_tarjeta_orig", "123456");
			jsonRequest.put("monto_recarga", formatter.format(monto_recarga).replace(",", "."));
			jsonRequest.put("bill_val", bill_val);
			jsonRequest.put("mon_val", mon_val);
			jsonRequest.put("monto_devuelto", formatter.format(monto_devuelto).replace(",", "."));
			jsonRequest.put("monto_devolver", formatter.format(monto_devolver).replace(",", "."));
//			jsonRequest.put("cabecera_voucher", "");//upc
			jsonRequest.put("desc_servicio_voucher","PRODUCTO");
			jsonRequest.put("cliente_voucher","NRO DE PEDIDO");
//			jsonRequest.put("desc_costo_voucher","TOTAL A PAGAR");
//			jsonRequest.put("desc_pago_voucher","MONTO PAGADO");
			jsonRequest.put("data","transaction_code;aplicacion_trx;service_code;desc_service;cod_user;agencia;num_referencia;num_tarjeta;num_tarjeta_orig;monto_recarga;bill_val;mon_val;monto_devuelto;monto_devolver;desc_servicio_voucher;cliente_voucher");
			jsonRequest.put("terminal_serial_number",terminal);
			jsonRequest.put("trace_number",trace);

			LOGGER.info(trace + " INPUT cancelarPago " + jsonRequest.toString());
			HttpEntity<String> request = new HttpEntity<String>(jsonRequest.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi, request, HashMap.class);
			data = result.getBody();
			if (!data.isEmpty()) {
				data.put("estado", "0");
			} else {
				data.put("estado", "-1");
			}
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT cancelarPago " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR cancelarPago", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}

	@Override
	public HashMap<String, String> consultaRucPortales(String trace, String terminal, String ruc, String operationType,
			String name, String street1, String postalCode, String city) {
		// TODO Auto-generated method stub
		LOGGER.info(trace + " INICIO consultaRucPortales");
		TpComponenteLog componenteLog = new TpComponenteLog(nroTerminal, trace, "consultaRucPortales");
		componenteLog.sethTxServiceInput(UtilWeb.getHoraActual());
		componenteLog.setcTxTypeService(UtilWeb.SERVICE_HIPERCENTER);
		HashMap<String, String> data = new HashMap<String, String>();
		try {
			RestTemplate restTemplate = new RestTemplate();
			JSONObject jsonRequest = new JSONObject();
			
			jsonRequest.put("transaction_code", "TRX_RUC_PORTALES");
			jsonRequest.put("aplicacion_trx", aplHcenter);
			jsonRequest.put("ruc",ruc);
			jsonRequest.put("operationType",operationType);
			jsonRequest.put("name", name);
			jsonRequest.put("street1", street1);
			jsonRequest.put("postalCode", postalCode);
			jsonRequest.put("city", city);
			jsonRequest.put("data","transaction_code;aplicacion_trx;ruc;operationType;name;street1;postalCode;city");
			jsonRequest.put("terminal_serial_number",terminal);
			jsonRequest.put("trace_number",trace);

			LOGGER.info(trace + " INPUT cancelarPago " + jsonRequest.toString());
			HttpEntity<String> request = new HttpEntity<String>(jsonRequest.toString(), UtilWeb.getHeader());
			ResponseEntity<HashMap> result = restTemplate.postForEntity(urlApi, request, HashMap.class);
			data = result.getBody();
			if (!data.isEmpty()) {
				data.put("estado", "0");
			} else {
				data.put("estado", "-1");
			}
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.info(trace + " OUTPUT consultaRucPortales " + result.toString());
		} catch (Exception e) {
			data.put("estado", "-1");
			data.put("descripcion", "Error conexion");
			componenteLog.setnTxError(0);
			componenteLog.sethTxServiceOutput(UtilWeb.getHoraActual());
			LOGGER.error(trace + "ERROR consultaRucPortales", e);
		} finally {
			componenteService.saveComponente(componenteLog);
		}
		return data;
	}


}
