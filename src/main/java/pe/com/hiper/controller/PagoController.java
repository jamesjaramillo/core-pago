package pe.com.hiper.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.hiper.business.PagoBussines;

@RestController
@RequestMapping(path = "/evaPago/api/v1/pagos")
public class PagoController {

	@Autowired
	@Qualifier("PagoBussinesFujitsu")
	PagoBussines pagoBussines;
	
	@Autowired
	@Qualifier("PagoBussinesNV200")
	PagoBussines pagoBussinesNv200;
	
	@Value("${kiosco.tipoAceptador}")
	private String tipoAceptador;
	
	private PagoBussines getBussines() {
		if(tipoAceptador.equals("4")) {
			return pagoBussines;
		}
		else {
			return pagoBussinesNv200;
		}
	}
	
	@PostMapping(path = "/habilitarIngresoEfectivo", consumes = "application/json", produces = "application/json")
	public HashMap<String, String> habilitarIngresoEfectivo(@RequestBody HashMap<String, Object> input) {
		//System.out.println("habilitarIngresoEfectivo -> " + input);
		return getBussines().habilitarIngresoEfectivo(input.get("idProducto").toString(),
				input.get("idCliente").toString(), Double.parseDouble(input.get("costoServicio").toString()),
				Integer.parseInt(input.get("timeOut").toString()), input.get("trace").toString(),
				Integer.parseInt(input.get("recarga").toString()),
				Integer.parseInt(input.get("flagInicio").toString()));
	}

	@PostMapping(path = "/habilitarIngresoDeBilletes", consumes = "application/json", produces = "application/json")
	public HashMap<String, Object> habilitarIngresoDeBilletes(@RequestBody HashMap<String, Object> input) {
		System.out.println("habilitarIngresoDeBilletes -> " + input);
		return getBussines().habilitarIngresoDeBilletes(input.get("trace").toString());
	}

	// desactivarDispositivos
	@PostMapping(path = "/detenerIngresoEfectivo", consumes = "application/json", produces = "application/json")
	public HashMap<String, String> detenerIngresoEfectivo(@RequestBody HashMap<String, Object> input) {
		return getBussines().detenerIngresoEfectivo(input.get("trace").toString());
	}

	@PostMapping(path = "/consultarEfectivoIngresado", consumes = "application/json", produces = "application/json")
	public HashMap<String, Object> consultarEfectivoIngresado(@RequestBody HashMap<String, Object> input) {
		return getBussines().consultarEfectivoIngresado(input.get("trace").toString());
	}

	@PostMapping(path = "/procesarPago", consumes = "application/json", produces = "application/json")
	public HashMap<String, Object> procesarPago(@RequestBody HashMap<String, Object> input) {
		return getBussines().procesarPago(input.get("trace").toString(), input.get("tipo_pago").toString(),
				input.get("nombre_cliente").toString(), input.get("nroDocumento").toString(),
				input.get("telefono").toString(), input.get("correo").toString(),
				input.get("nombre_producto").toString(), input.get("nroDocumento").toString(),
				input.get("flagTPP").toString(), input.get("codigoVerificacion").toString());
	}

	@PostMapping(path = "/cancelarUltimoComando", consumes = "application/json", produces = "application/json")
	public HashMap<String, String> cancelarUltimoComando(@RequestBody HashMap<String, Object> input) {
		return getBussines().cancelarUltimoComando(input.get("trace").toString());
	}

	@PostMapping(path = "/entregarDinero", consumes = "application/json", produces = "application/json")
	public HashMap<String, String> entregarDinero(@RequestBody HashMap<String, Object> input) {
		return getBussines().entregarDinero(input.get("trace").toString(), Integer.parseInt(input.get("trace").toString()), false);
	}
	
	@PostMapping(path = "/cancelarPago", consumes = "application/json", produces = "application/json")
	public HashMap<String, String> cancelarPago(@RequestBody HashMap<String, String> input) {
		return getBussines().cancelarPago(input.get("trace"), input.get("nombre_producto"), input.get("codigoVerificacion"));
	}
}
