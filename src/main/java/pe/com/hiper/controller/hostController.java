package pe.com.hiper.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.hiper.business.HostClienteBusiness;

@RestController
@RequestMapping(path = "/evaPago/api/v1/hostCliente")
public class hostController {

	@Autowired
	HostClienteBusiness hostClienteBus;

	@PostMapping(path = "/obtenerCostoProducto", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> obtenerCostoProducto(@RequestBody HashMap<String, Object> input) throws Exception {

		HashMap<String, String> data = hostClienteBus.obtenerCostoProducto(input.get("trace").toString(),
				input.get("nroTicket").toString(), input.get("nroCupon").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/pagoPortales", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> pagoPortales(@RequestBody HashMap<String, String> input) throws Exception {

		HashMap<String, String> data = hostClienteBus.pagoPortales(input.get("trace").toString(),
				input.get("nroTicket").toString(), input.get("nroCupon").toString(),
				input.get("fechaIngreso").toString(), input.get("fechaSalida").toString(),
				input.get("stacionEntrada").toString(), input.get("monto").toString(), input.get("impuesto").toString(),
				input.get("montoTotal").toString(), input.get("tipoPago").toString(),
				input.get("numOperacion").toString(), input.get("numAutorizacion").toString(),
				input.get("cardNumberEnc").toString(), input.get("cardRead").toString(),
				input.get("flagNtaCredito").toString(), input.get("montoNtaCredito").toString(),
				input.get("idTransaccion").toString(), input.get("fechaHoraTrx").toString());

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/cancelarPagoPortales", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> cancelarPagoPortales(@RequestBody HashMap<String, String> input) throws Exception {

		HashMap<String, String> data = hostClienteBus.cancelarPagoPortales(input.get("trace"), input.get("nroTicket"),
				input.get("nroCupon"), input.get("fechaIngreso"), input.get("fechaSalida"), input.get("stacionEntrada"),
				input.get("monto"), input.get("impuesto"), input.get("montoTotal"));

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/voucherPortales", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> voucherPortales(@RequestBody HashMap<String, String> input) throws Exception {

		HashMap<String, String> data = hostClienteBus.voucherPortales(input.get("trace"), input.get("service_name"),
				input.get("payPassBalance"), input.get("payPassConsumo"), input.get("payPassAmountOld"),
				input.get("payPassCard"), input.get("payPassName"), input.get("payPassDni"), input.get("payPassTrace"));

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

	@PostMapping(path = "/consultaRucPortales", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> consultaRucPortales(@RequestBody HashMap<String, String> input) throws Exception {

		HashMap<String, String> data = hostClienteBus.consultaRucPortales(input.get("trace"), input.get("ruc"),
				input.get("tipoOperacion"), input.get("nombre"), input.get("calle"), input.get("codigoPostal"),
				input.get("ciudad"));

		return ResponseEntity.status(HttpStatus.OK).body(data);
	}

}
