package pe.com.hiper.DTO;

import java.util.Date;


public class MontoPagarDTO {

	private String cTrace;
	private double nCostoServicio;
	private String cCodCliente;
	private Date dFechaIngreso;
	private String nCodProducto;
	
	
	public String getcTrace() {
		return cTrace;
	}
	public void setcTrace(String cTrace) {
		this.cTrace = cTrace;
	}
	public double getnCostoServicio() {
		return nCostoServicio;
	}
	public void setnCostoServicio(double nCostoServicio) {
		this.nCostoServicio = nCostoServicio;
	}
	public String getcCodCliente() {
		return cCodCliente;
	}
	public void setcCodCliente(String cCodCliente) {
		this.cCodCliente = cCodCliente;
	}
	public Date getdFechaIngreso() {
		return dFechaIngreso;
	}
	public void setdFechaIngreso(Date dFechaIngreso) {
		this.dFechaIngreso = dFechaIngreso;
	}
	public String getnCodProducto() {
		return nCodProducto;
	}
	public void setnCodProducto(String nCodProducto) {
		this.nCodProducto = nCodProducto;
	}
	
	
}
