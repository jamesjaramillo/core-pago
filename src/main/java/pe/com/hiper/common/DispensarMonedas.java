package pe.com.hiper.common;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pe.com.hiper.service.EvaCoreService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



public class DispensarMonedas {
	private static final Logger LOGGER = LogManager.getLogger(DispensarMonedas.class);
	
	public HashMap<String, Object> procesarVuelto(String trace, String moneda, Double monto, EvaCoreService smartHopper, int maximoHopper, int totalHopper, int denominacionHopper, boolean isPrimero){
		LOGGER.info(trace+" "+"INGRESO METODO procesarVuelto ");
		LOGGER.info(trace+" "+"INPUT =  "+moneda);
		LOGGER.info(trace+" "+"INPUT =  "+monto);
		JSONObject monedasDisponibles=smartHopper.obtenerMonedasSH(trace);
		LOGGER.info(trace+" "+"Monedas disponibles = "+monedasDisponibles);
		HashMap<String, Object> response=new HashMap<String, Object>();
		//int denominador=100;
		int denominador=1;
		Double montoPendiente=0.0;
		Double monedaMinima=0.1;
		BigDecimal montoDecimal=new BigDecimal(0);
		if((maximoHopper>0 && totalHopper>0) || !isPrimero){
			montoDecimal=new BigDecimal(monto-Math.floor(monto)).setScale(2, RoundingMode.HALF_UP);
			monto=Math.floor(monto);
			LOGGER.info(trace+" "+"Monto entero = "+monto);
			LOGGER.info(trace+" "+"Monto decimal = "+montoDecimal);
		}
		
		LOGGER.info(trace+" "+"INPUT NUEVO MONTO=  "+monto);
		if(moneda.equals("COP")){
			denominador=100;
			monedaMinima=100.0;
		}
		try {
			response.put("estado", "1");
			response.put("descripcion", "No se entrego vuelto");
			JSONArray listaMonedas=monedasDisponibles.getJSONArray("monedas");
			if(monedasDisponibles.get("estado").equals("0") && listaMonedas.length()>0 ){
				
				System.out.println("***listaMonedas***");
				System.out.println(listaMonedas);
				Double totalMonedas=getMontoTotal(listaMonedas, moneda);
				LOGGER.info(trace+" "+"totalMonedas = "+totalMonedas);
				Double montoDispensar=monto;
				if(moneda.equals("COP"))
					montoDispensar=montoDispensar-(monto%monedaMinima);
				
				if(monto>totalMonedas){
					montoDispensar=totalMonedas;
				}
				LOGGER.info(trace+" "+"montoDispensar = "+montoDispensar);
				JSONObject objtoVuelto = smartHopper.dispensarMonedasSH(trace, (montoDispensar / denominador));
				System.out.println("***MONEDAS DISPENSADAS***");
				LOGGER.info(trace+" "+"objtoVuelto = "+objtoVuelto);
				/*if(objtoVuelto.get("estado").equals("5")){
					JSONObject monedasFinales=smartHopper.obtenerMonedasSMH(trace);
				}*/
				Double montoDispensado=0.0;
				if(!objtoVuelto.get("estado").equals("6")){
					montoDispensado=getMontoTotal(objtoVuelto.getJSONArray("monedas"), moneda);
				}
				
				LOGGER.info(trace+" "+"montoDispensado = "+montoDispensado);
				Double montoRestante=monto-montoDispensado;	
				LOGGER.info(trace+" "+"montoRestante = "+montoRestante);
				HashMap<Double, Integer> monedasDispensada=getMonedasDispensadas(objtoVuelto.getJSONArray("monedas"),moneda);
				System.out.println("***MONEDAS DISPENSADAS FORMAT***");
				System.out.println(monedasDispensada);
				response.put("estado", "0");
				response.put("descripcion", "Proceso vuelto ok");
				response.put("dispensado", monedasDispensada);
				response.put("montoPendiente", montoRestante);
				 /**logica para hopper**/
				
				if(maximoHopper>0){
					montoDecimal=new BigDecimal(montoDecimal.doubleValue() + montoRestante).setScale(2, RoundingMode.HALF_UP);
					//int cantidadMonedas= (int) (montoDecimal.doubleValue()*10);
					LOGGER.info(trace+" "+"montoDecimalm para el hopper = "+montoDecimal);
					int cantidadMonedas=(int) Math.floor(((montoDecimal.doubleValue()*100)/denominacionHopper));
					LOGGER.info(trace+" "+"cantidadMonedas para el hopper = "+cantidadMonedas);
					int montoRetante=cantidadMonedas;
					if(maximoHopper>totalHopper){
						maximoHopper=totalHopper;
					}
					if(cantidadMonedas>maximoHopper){
						cantidadMonedas=maximoHopper;
					}
					LOGGER.info(trace+" "+"HOPPER CANTIDAD MONEDAS A DISPENSAR = "+cantidadMonedas);
					if(cantidadMonedas>0){
						try {							
						
							//String dispensar = smartHopper.dispensarMonedas("1", cantidadMonedas, trace);
							HashMap<String, String> dispensar=smartHopper.getDispensarHopper(trace, "0", cantidadMonedas);
							LOGGER.info(trace+" "+"RESULTADO DE DISPENSAR MONEDAS -> "+dispensar.get("descripcion"));
							int monNodDevueltos=Integer.parseInt(dispensar.get("descripcion").toString().substring(0,2).trim().replace(" ", ""));
							if(monNodDevueltos>=0){						
								
								int monedasDispensadas=cantidadMonedas-monNodDevueltos;
								response.put("hopper", monedasDispensadas);
								//double totalDispensado=new BigDecimal(monedasDispensadas).divide(new BigDecimal(10)).setScale(2, RoundingMode.HALF_UP).doubleValue();double totalDispensado=new BigDecimal(monto*dDenomimacion).setScale(2, RoundingMode.HALF_UP).doubleValue();
								double dDenomimacion=(double)denominacionHopper/100;
								double totalDispensado=new BigDecimal(monedasDispensadas*dDenomimacion).setScale(2, RoundingMode.HALF_UP).doubleValue();
								//if(montoDecimal.doubleValue()-totalDispensado>0){
								response.put("montoPendiente", montoDecimal.doubleValue()-totalDispensado);
								//}
								LOGGER.info(trace+" "+"RESULTADO montoDecimal.doubleValue()-totalDispensado -> "+(montoDecimal.doubleValue()-totalDispensado));
							}
							else{
								response.put("montoPendiente", montoDecimal.doubleValue());
							}
							
							
							//double montoRestante2=new BigDecimal(montoDecimal.doubleValue()).setScale(2, RoundingMode.HALF_UP).doubleValue();
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();							
						}
						
					}
					else{
						response.put("montoPendiente", montoDecimal.doubleValue());
					}
				}
				else{
					response.put("montoPendiente", montoRestante+montoDecimal.doubleValue());
				}
				
			}
			else{
				response.put("montoPendiente", monto);
			}
		} catch (Exception e) {
			// TODO Auto-generated atch block
			response.put("montoPendiente", monto);
			response.put("estado", "-1");
			response.put("descripcion", "Error en el proceso");
			e.printStackTrace();
		}	
		
		return response;
	}
	
	private Double getMontoTotal(JSONArray listaMonedas, String moneda){
		Double montoTotal=0.0;
		int denominador=1;
		if(moneda.equals("COP")){
			denominador=100;
		}
		try {			
			if(listaMonedas.length()>0){
				for (int i = 0; i < listaMonedas.length(); i++) {
					System.out.println("denominacion y cantidad");
					String denomacion=listaMonedas.getJSONObject(i).getString("denominacion").replace(moneda+"-","");
					montoTotal+=Double.parseDouble(denomacion) * (listaMonedas.getJSONObject(i).getInt("cantidad"));
					
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		montoTotal=new BigDecimal(montoTotal).setScale(2, RoundingMode.HALF_UP).doubleValue();
		return montoTotal * denominador;
	}
	
	private HashMap<Double, Integer> getMonedasDispensadas(JSONArray listaMonedas, String moneda){
		HashMap<Double, Integer> dataList=new HashMap<Double, Integer>();
		int denominador=1;
		if(moneda.equals("COP")){
			denominador=100;
		}
		try {			
			if(listaMonedas.length()>0){
				for (int i = 0; i < listaMonedas.length(); i++) {
					
					String denomacion=listaMonedas.getJSONObject(i).getString("denominacion").replace(moneda+"-","");
					System.out.println("denomacion "+denomacion);
					Double denominacion = Double.parseDouble(denomacion) *  (double)denominador;
					int cantidad = listaMonedas.getJSONObject(i).getInt("cantidad");
					System.out.println("denominacion "+denominacion+" y cantidad "+cantidad);
					dataList.put(denominacion, cantidad);
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dataList;
	}
	
	
}
