package pe.com.hiper.common;

public class Constantes {
	private final static String URL_BILLETERO = "api_billetero";
	private final static String URL_MONEDERO = "api_monedero";
	private final static String URL_SMARTHOPPER = "api_smarthopper";
	private final static String URL_HOPPER = "api_hopper";
	private final static String URL_IMPRESORA = "api_impresora";
	private final static String URL_LUCES = "api_luces";
	private final static String URL_SENSOR = "api_sensor";
	private final static String URL_CASTLE = "api_castle";

	/*
	 * public final static String SERVICIO_BILLETERO_ACEPTAR =
	 * "/ServicioBilletero/aceptarBilletes/"; public final static String
	 * SERVICIO_BILLETERO_CANCELAR = "/ServicioBilletero/cancelarBilletes/"; public
	 * final static String SERVICIO_BILLETERO_FINALIZAR =
	 * "/ServicioBilletero/finalizarAceptacion/"; public final static String
	 * SERVICIO_BILLETERO_DISPENSAR = "/ServicioBilletero/dispensarBilletes/";
	 * public final static String SERVICIO_BILLETERO_VACIAR =
	 * "/ServicioBilletero/vaciarPayout/"; public final static String
	 * SERVICIO_BILLETERO_RESETEAR = "/ServicioBilletero/resetearEquipo/"; public
	 * final static String SERVICIO_BILLETERO_SOLCASHIN =
	 * "/ServicioBilletero/solucionCashInActive/"; public final static String
	 * SERVICIO_BILLETERO_CANCEL_COMMAND =
	 * "/ServicioBilletero/cancelarUltimoComando/";
	 * 
	 * public final static String SERVICIO_BILLETERO_ESTADO =
	 * "/ServicioBilletero/obtenerEstadoEquipo/"; public final static String
	 * SERVICIO_BILLETERO_HABILITAR = "/ServicioBilletero/habilitarBilletes/";
	 * public final static String SERVICIO_BILLETERO_CONSULTAR_COMP =
	 * "/ServicioBilletero/consultarComponente/"; public final static String
	 * SERVICIO_BILLETERO_CONSULTAR_COMPS =
	 * "/ServicioBilletero/consultarComponentes/"; public final static String
	 * SERVICIO_BILLETERO_ACTUALIZAR_COMPS =
	 * "/ServicioBilletero/actualizarComponentes/"; public final static String
	 * SERVICIO_BILLETERO_BILLETES_COMP =
	 * "/ServicioBilletero/getBilletesxComponente/"; public final static String
	 * SERVICIO_BILLETERO_CONFIGURAR_COMP =
	 * "/ServicioBilletero/configurarComponente/"; public final static String
	 * SERVICIO_BILLETERO_BILLETES_INGRE =
	 * "/ServicioBilletero/getBilletesIngresados/";
	 * 
	 * // SERVICIOS PARA EL SMARTHOPPER public final static String
	 * SERVICIO_SMH_ESTADO = "/ServicioSmartHopper/estadoDispositivo/"; public final
	 * static String SERVICIO_SMH_INICIAR = "/ServicioSmartHopper/iniciarHopper/";
	 * public final static String SERVICIO_SMH_HABILITAR =
	 * "/ServicioSmartHopper/habilitarHopper/"; public final static String
	 * SERVICIO_SMH_DESHABILITAR = "/ServicioSmartHopper/deshabilitarHopper/";
	 * public final static String SERVICIO_SMH_OBTENER_MONEDAS =
	 * "/ServicioSmartHopper/obtenerMonedas/"; public final static String
	 * SERVICIO_SMH_REINICIAR = "/ServicioSmartHopper/resetearEquipo/"; public final
	 * static String SERVICIO_SMH_DISPENSAR_MONEDAS =
	 * "/ServicioSmartHopper/dispensarMonedas/"; public final static String
	 * SERVICIO_SMH_DISPENSAR_MONTO = "/ServicioSmartHopper/dispensarMonto/";
	 * 
	 * public final static String SERVICIO_SMH_AGREGAR_MONEDA =
	 * "/ServicioSmartHopper/agregarMonedas/"; public final static String
	 * SERVICIO_SMH_VACIAR = "/ServicioSmartHopper/vaciarHopper/"; public final
	 * static String SERVICIO_SMH_RECONEXION =
	 * "/ServicioSmartHopper/reiniciarConexion/";
	 * 
	 * // SERVICIO PARA LA IMPRESORA public final static String
	 * SERVICIO_IMPRESORA_ESTADO = "/ServicioImpresoraTermica/testearImpresora/";
	 * public final static String SERVICIO_IMPRESORA_IMPRIMIR =
	 * "/ServicioImpresoraTermica/imprimir/";
	 * 
	 * // SERVICIO PARA EL MONEDERO public static final String
	 * SERVICIO_MONEDERO_ESTADO = "/ServicioAceptador/obtenerEstadoAceptador";
	 * public static final String SERVICIO_MONEDERO_INICIAR =
	 * "/ServicioAceptador/habilitarAceptador"; public static final String
	 * SERVICIO_MONEDERO_DETENER = "/ServicioAceptador/bloquearAceptador"; public
	 * static final String SERVICIO_MONEDERO_DENOMINACIO =
	 * "/ServicioAceptador/obtenerMonedasIngresadas";
	 * 
	 * // SERVICIO PARA EL SCROW O DIVERTOR public static final String
	 * SERVICIO_SCROW_TESTEAR = "/ServicioScrow/testear"; public static final String
	 * SERVICIO_SCROW_ACEPTAR = "/ServicioScrow/aceptar"; public static final String
	 * SERVICIO_SCROW_CANCELAR = "/ServicioScrow/devolver";
	 * 
	 * // SERVICIO LUCES public static final String SERVICIO_LUCES_ENCENDER =
	 * "/ServicioLuces/encenderLuces"; public static final String
	 * SERVICIO_LUCES_APAGAR = "/ServicioLuces/apagarLuces";
	 * 
	 * // SERVICIO SENSOR public static final String SERVICIO_SENSOR_ESTADO =
	 * "/ServicioSensores/obtenerEstadoSensor";
	 * 
	 * // SERVICIO HOPPER public static final String SERVICIO_HOPPER_ESTADO =
	 * "/ServicioHopper/obtenerEstadoEquipo"; public static final String
	 * SERVICIO_HOPPER_DISPENSAR = "/ServicioHopper/dispensar";
	 * 
	 * // SERVICIO CASTLE public static final String SERVICIO_CASTLE_ESTADO =
	 * "/ServicioCastle/estadoCastle"; public static final String
	 * SERVICIO_CASTLE_SINCRONIZACION = "/ServicioCastle/sincronizacionCastle";
	 * public static final String SERVICIO_CASTLE_OBTENER_DATOS =
	 * "/ServicioCastle/obtenerDatosCastle"; public static final String
	 * SERVICIO_CASTLE_VENTA = "/ServicioCastle/realizarVenta"; public static final
	 * String SERVICIO_CASTLE_CONFIRMAR = "/ServicioCastle/confirmarOk"; public
	 * static final String SERVICIO_CASTLE_CONFIRMAR_CANCEL =
	 * "/ServicioCastle/confirmarCancel";
	 */

	// SERVICIO PARA EL MONEDERO
	public static final String EVACORE_MONEDERO_ESTADO = "monedero/obtenerEstadoAceptador";
	public static final String EVACORE_MONEDERO_INICIAR = "monedero/habilitarMonedero";
	public static final String EVACORE_MONEDERO_DETENER = "monedero/deshabilitarMonedero";
	public static final String EVACORE_MONEDERO_DENOMINACIO = "monedero/obtenerDenominacion";

	// SERVICIO PARA EL BILLETERO
	public static final String EVACORE_BILLETERO_ESTADO = "monedero/obtenerEstadoAceptador";
	public static final String EVACORE_BILLETERO_ACEPTAR = "billetero/aceptarBilletes";
	public static final String EVACORE_BILLETERO_FINALIZARSCROW = "billetero/finalizarScrow";
	public static final String EVACORE_BILLETERO_CANCELAR_UTL = "billetero/cancelarUltimoComando";
	public static final String EVACORE_BILLETERO_DISPENSAR = "billetero/dispensarBilletes";
	public static final String EVACORE_BILLETERO_CANCELAR_SCROW = "billetero/cancelarScrow";
	public static final String EVACORE_BILLETERO_BILLETES_INGRE = "billetero/billetesIngresado";
	
	//SERVICIO SMARTHOPPER
	public static final String EVACORE_SMARTH_AGREGAR = "smartHopper/agregarMonedas";
	public static final String EVACORE_SMARTH_ESTADO = "smartHopper/estadoSMH";
	public static final String EVACORE_SMARTH_DISPENSAR = "smartHopper/dispensarMonto";
	public static final String EVACORE_SMARTH_VACIAR= "smartHopper/vaciarSmartHopper";
	public static final String EVACORE_SMARTH_OBTENER_MON= "smartHopper/obtenerMonedas";
	
	public static final String EVACORE_HOPPER_DISPENSAR= "hopper/dispensarHopper";
	
	
	//SERVICIO SMARTHOPPER
		public static final String EVACORE_ESCROW_ACEPTAR = "scrow/aceptarScrow";
		public static final String EVACORE_ESCROW_DEVOLVER = "scrow/devolverScrow";
	

}
