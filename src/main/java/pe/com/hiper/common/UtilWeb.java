package pe.com.hiper.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import pe.com.hiper.model.Moneda;

public class UtilWeb {

	/* SERVICIOS DEL EVACORE */
	public static final String EVACORE_MONEDERO_INICIAR = "/evaCore/api/v1/monedero/habilitarMonedero";
	/**/

	public final static String SERVICE_HIPERCENTER = "HIPERCENTER";
	public final static String SERVICE_API_CORE = "APICORE";

	public static boolean KIOSCO_EN_USO = false;
	public static boolean KIOSCO_EN_RED = true;

	public static String KEY1 = "1A3F5B68A93C34E5";
	public static String KEY2 = "B61A3F53C8A934E5";
	public static String KEY3 = "153C3A3F4E5B68A9";

	public static HashMap<String, Object> H_MONTO_INGRESADO;
	public static ArrayList<Moneda> VUELTO_BILLETES = new ArrayList<Moneda>();
	public static double MONTO_INGRESADO;
	public static int TOTAL_MONEDAS_INGRESADAS;
	public static String TRACE_PROCESADO = "";
	public static String NRO_TICKET = "";
	public static double VUELTO_MONEDAS = 0.0;

	public static HttpHeaders getHeader() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		return headers;
	}

	public static String getHoraActual() {
		Locale currentLocale = new Locale("es", "PE");
		Date today = new Date();
		DateFormat formatter = new SimpleDateFormat("HH:mm:ss:SSS", currentLocale);
		String result = formatter.format(today);
		return result;
	}

	public static String getFechaActual() {
		Locale currentLocale = new Locale("es", "PE");
		Date today = new Date();
		DateFormat formatter = new SimpleDateFormat("ddMMyyyy", currentLocale);
		String result = formatter.format(today);
		return result;
	}

	public static int minutosDiferencia(String horaInicio, String horaFin) {

		DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		int minutos = 0;
		try {
			Date dateInicio = format.parse(horaInicio);
			Date dateFin = format.parse(horaFin);
			Long diferencia = dateFin.getTime() - dateInicio.getTime();
			int segundos = diferencia.intValue() / 1000;

			minutos = segundos / 60;
			int resto = segundos % 60;

			if (resto > 30)
				minutos++;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return minutos;
	}

}
